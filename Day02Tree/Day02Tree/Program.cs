﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02Tree
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.Write("How big does your tree needs to be? ");
                string countStr = Console.ReadLine();
                int count = Convert.ToInt32(countStr);
                // int count = int.Parse(countStr);
                if (count < 1)
                {
                    Console.WriteLine("Error: you must generate 1 or more numbers");
                    return;
                }

               int i, j, k;
                for (i = 1; i <= count; i++)
                {
                    for (j = i; j < count; j++)
                    {
                        Console.Write(" ");
                    }
                    for (k = 1; k < (i * 2); k++)
                    {
                        Console.Write("*");
                    }
                    Console.WriteLine();
                }
               /* int i, j, k;
                for (i = count; i >= 1; i--)
                {
                    for (j = 1; j < i; j++)
                    {
                        Console.Write(" ");
                    }
                    for (k = count; k > i; k--)
                    {
                        Console.Write("*");
                    }
                    Console.WriteLine();
                }
                Console.ReadLine();*/
            }
            finally
            {
                Console.WriteLine("Press any key to end");
                Console.ReadKey();
            }
            
        }
    }
}
