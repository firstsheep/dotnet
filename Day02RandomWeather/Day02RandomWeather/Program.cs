﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02RandomWeather
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();           
            int output = rnd.Next(-31, 31);
            Console.WriteLine("the number is " + output);
            if (output <= -15)
            {
                Console.WriteLine("Very very cold");
            }else if(output>-15 && output < 0)
            {
                Console.WriteLine("Freezing already");
            }
            else if (output >= 0 && output <= 15)
            {
                Console.WriteLine("Spring or Fall");
            }
            else if (output > 15)
            {
                Console.WriteLine("That's what I like");
            }

            Console.ReadKey();


        }
    
    }
}
