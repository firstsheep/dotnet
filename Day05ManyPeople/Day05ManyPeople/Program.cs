﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05ManyPeople
{
    class Person
    {
        /*
        public int Random
        {
            get {
                return new Random().Next(1,100);
            }
        } */
        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2)
                {
                    throw new ArgumentOutOfRangeException("Name must be at least 2 characters long");
                }
                _name = value;
            }
        }

        // Age must be between 0-150 (both inclusive)
        private int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new ArgumentOutOfRangeException("Age must be between 0-150");
                }
                _age = value;
            }
        }
        public override string ToString()
        {
            // return base.ToString;
            return string.Format("Person: {0} is {1} y.o", Name, Age);

            // return "Person: " + Name + " is " + Age + " from " + City;
        }

    }
    class Teacher : Person
    {
        public Teacher(string name, int age, string field, int yoe) : base(name, age)
        {
            this.Field = field;
            this.YOE = yoe;
        }
        private string _field;
        public string Field
        {
            get
            {
                return _field;
            }
            set
            {
                if (value.Length < 2)
                {
                    throw new ArgumentOutOfRangeException("Field must be at least 2 characters long");
                }
                _field = value;
            }
        }
        private int _YOE;
        public int YOE
        {
            get
            {
                return _YOE;
            }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentOutOfRangeException("YOE must be between 0-100");
                }
                _YOE = value;
            }
        }
        public override string ToString()
        {
            // return base.ToString;
            return string.Format("Teacher: {0} is {1} y.o, has been teaching {2} for {3} years", Name, Age, Field, YOE);

            // return "Person: " + Name + " is " + Age + " from " + City;
        }
    }
    class Student : Person
    {
        public Student(string name, int age, string program, double gpa) : base(name, age)
        {
            this.Program = program;
            this.GPA = gpa;
        }
        private string _program;
        public string Program
        {
            get
            {
                return _program;
            }
            set
            {
                if (value.Length < 2)
                {
                    throw new ArgumentOutOfRangeException("_program must be at least 2 characters long");
                }
                _program = value;
            }
        }
        private double _GPA;
        public double GPA
        {
            get
            {
                return _GPA;
            }
            set
            {
                if (value < 0 || value > 4.0)
                {
                    throw new ArgumentOutOfRangeException("GPA must be between 0-4.0");
                }
                _GPA = value;
            }
        }
        public override string ToString()
        {
            // return base.ToString;
            return string.Format("Student: {0} is {1} studying {2} with GPA {3}", Name, Age, Program, GPA);

            // return "Person: " + Name + " is " + Age + " from " + City;
        }
    }
    class Program
    {
        const string file_path = @"..\..\..\people.txt";
        static List<Person> people = new List<Person>();
        
        static void Main(string[] args)
        {
            try
            {

                string[] item;
                string name1;
                int age1;
                string type;
                string[] lines = File.ReadAllLines(file_path);
                foreach (string l in lines)
                {
                    try
                    {
                        item = l.Split(';');
                     /*   if (item.Length < 3 || item.Length > 5)
                        {
                            throw new InvalidDataException("Line must have three to five data items: " + l);
                        }
                        */
                        type = item[0];
                        name1 = item[1];
                        age1 = int.Parse(item[2]);

                    //int.tryParse
                        switch (type)
                        {
                            case "Person":
                                if (item.Length != 3 )
                                {
                                    Console.WriteLine("Line must have three items: " + l);
                                    continue;
                                }
                                Person p = new Person(name1, age1);
                                people.Add(p);
                                
                                break;
                            case "Teacher":
                                string field = item[3];
                                int teachingYears = int.Parse(item[4]);
                                Person t = new Teacher(name1, age1, field, teachingYears);                               
                                people.Add(t);                               
                                break;
                            case "Student":
                                string program = item[3];
                                double gpa = double.Parse(item[4]);
                                Person stu = new Student(name1, age1, program, gpa);                              
                                people.Add(stu);
                                break;
                            default:
                                // FIXME TODO
                                Console.WriteLine("Error,skipping,I don't know how to make {0} in line{1}", type, l);
                                break;


                        }



                    }
                    catch (Exception ex)
                    {
                        if (ex is FormatException || ex is OverflowException || ex is ArgumentOutOfRangeException || ex is InvalidDataException)
                        {
                            Console.WriteLine("Error occured: " + ex.Message);
                        }
                        else
                        {
                            throw ex;
                        }
                    }
                }
                foreach (Person everyP in people)
                {
                    Console.WriteLine(everyP);
                }
                //loop to display only person
                foreach (Person everyP in people)
                {
                    Type t = everyP.GetType();
                    if (t.Equals(typeof(Person)))
                        Console.WriteLine("Person's Name:");
                        Console.WriteLine(everyP.Name);
                }
                //loop to display only Student
                foreach (Person everyP in people)
                {
                    Type t = everyP.GetType();
                    if (t.Equals(typeof(Student)))
                        Console.WriteLine("Student's Name:");
                    Console.WriteLine(everyP.Name);
                }
                //loop to display only teacher
                foreach (Person everyP in people)
                {
                    Type t = everyP.GetType();
                    if (t.Equals(typeof(Teacher)))
                        Console.WriteLine("Teacher's Name:");
                    Console.WriteLine(everyP.Name);
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            }
            finally
            {
                Console.ReadKey();
            }
        }

    }
}

