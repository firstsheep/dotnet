﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05Virtuals
{
    class A
    {
        public void Foo() { Console.WriteLine("A::Foo()"); }
    }

    class B : A
    {
        public void Foo() { Console.WriteLine("B::Foo()"); }
    }
    class Program
    {
        static void Main(string[] args)
        {
            A a;
            B b;

            a = new A();
            b = new B();
            a.Foo();  // output --> "A::Foo()"
            b.Foo();  // output --> "B::Foo()"

           

            A ab = b; //down-cast safe
            ab.Foo();  // output --> "A::Foo()"  in JAVA:"B::Foo()"
            Console.ReadKey();
        }
    }
}
