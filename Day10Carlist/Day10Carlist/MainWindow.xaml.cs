﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day10Carlist
{
    public class Car
    {
        public String Make;
        public String CarSize; //FIXME: change into Enum
        public String Features; //FIXME: Dictionary of Enum or string
        public String Plates;
        public double WeightTonnes;

        public override string ToString()
        {
            return String.Format("{0} is {1} with {2} reg {3}, {4}t", Make, CarSize, Features, Plates, WeightTonnes);
        }
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Car> carList = new List<Car>();
        bool hasUnsavedData = false;

        public MainWindow()
        {
            InitializeComponent();
            carList.Add(new Car() { Make = "Audi", CarSize = "Compact", Features = "", Plates = "AL8723", WeightTonnes = 1.39 });
            carList.Add(new Car() { Make = "Ford", CarSize = "Pickup", Features = "", Plates = "SDF523", WeightTonnes = 1.34 });
            carList.Add(new Car() { Make = "Benz", CarSize = "Van", Features = "", Plates = "23DFG7", WeightTonnes = 1.97 });

            lvCars.ItemsSource = carList;
        }

        private void FileExportSelected_MenuClick(object sender, RoutedEventArgs e)
        {
            if (lvCars.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select at least one car before exporting", "Car List", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }


            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Text files|*.txt|All Files|*.*";
            saveFileDialog1.Title = "Save an data File";
            saveFileDialog1.ShowDialog();
            List<string> linesList = new List<string>();
            if (saveFileDialog1.FileName != "")
            { //TODO: save data to file, semicolon separated


                foreach (Car car in lvCars.SelectedItems)
                {
                    linesList.Add(string.Format("{0};{1};{2};{3};{4};{5}", car.Make, car.CarSize, car.Features, car.Plates, car.WeightTonnes,
                                                Environment.NewLine));

                }
                try
                {
                    File.AppendAllLines(saveFileDialog1.FileName, linesList);

                    MessageBox.Show("Data saved");
                    hasUnsavedData = false;

                }
                catch (IOException ex)
                {
                    Console.WriteLine("Error accessing file: " + ex.Message);
                }
            }
        }
        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            //TODO: Warn user of "Ask to save unsaved data" Yes/No
            MessageBoxResult result = MessageBox.Show("Would you like to EXIT", "My car list", MessageBoxButton.OKCancel);
            try
            {
                switch (result)
                {
                    case MessageBoxResult.OK:
                        Close();
                        break;
                    case MessageBoxResult.Cancel:
                        MessageBox.Show("Nevermind then...", "My App");
                        break;

                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error accessing file: " + ex.Message);
            }
            //TODO: also handle window closing by pressing X on window frame

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!Regex.IsMatch(tbPlates.Text, @"^[A-Z0-9]{3,10}$"))
            {
                MessageBox.Show("Plates invalid, must 3-10 uppercase/digits");
                return;
            }
            string Make = coboMake.Text;
            if (Make == "")
            {
                MessageBox.Show("Please choose a make from the list", "My car list", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }
            string carSize = "";
            if (rbCompact.IsChecked == true)
            {
                carSize = "Compact";
            }
            else if (rbPickup.IsChecked == true)
            {
                carSize = "Pickup";
            }
            else if (rbVan.IsChecked == true)
            {
                carSize = "Van";
            }
            else
            {
                // WTF internal error
                MessageBox.Show("internal errors");
                return;
            }

            List<string> FeatureList = new List<string>();
            if (cbABS.IsChecked == true)
            {
                FeatureList.Add("ABS");
            }
            if (cbBulinton.IsChecked == true)
            {
                FeatureList.Add("Bulinton");
            }
            if (cbOther.IsChecked == true)
            {
                FeatureList.Add("Other");
            }
            string Features = String.Join(",", FeatureList);


            double Weight = (double)slWeight.Value;
            Weight = Math.Round(Weight, 2);

            Car car = new Car() { Make = Make, CarSize = carSize, Features = Features, Plates = tbPlates.Text, WeightTonnes = Weight };
            carList.Add(car);
            lvCars.Items.Refresh();
            hasUnsavedData = true;
            // clean up GUI inputs
            tbPlates.Text = "";
            coboMake.Text = "";
            rbCompact.IsChecked = true;
            cbABS.IsChecked = false;
            cbBulinton.IsChecked = false;
            cbOther.IsChecked = false;
            slWeight.Value = 0;

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (hasUnsavedData)
            {
                MessageBoxResult result = MessageBox.Show("unsaved data, exit?", "My car list", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
                if (result == MessageBoxResult.Cancel)
                {
                    e.Cancel = true;
                }
            }

        }
    }
}
