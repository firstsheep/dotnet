﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day14FirstDB
{
    class Program
    {
        const String DbConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\ipd\Documents\FirstDB.mdf;Integrated Security=True;Connect Timeout=30";
        static SqlConnection conn;
        static void Main(string[] args)
        {
            try
            {
                conn = new SqlConnection(DbConnectionString);
                conn.Open();
                // insert example
                try
                {
                    Console.Write("Enter Person Name: ");
                    string name = Console.ReadLine();
                    SqlCommand cmdInsert = new SqlCommand("INSERT INTO People(Name, Age) OUTPUT INSERTED.ID VALUES (@Name, @Age)", conn);
                    cmdInsert.Parameters.AddWithValue("Name", name);
                    cmdInsert.Parameters.AddWithValue("Age", new Random().Next(100));
                    int insertId = (int)cmdInsert.ExecuteScalar();
                   // cmdInsert.ExecuteNonQuery();
                    Console.WriteLine("Record inserted successfully with id=" + insertId);

                }
                catch (Exception ex)
                {
                    if (ex is SqlException || ex is InvalidCastException)
                    {
                        Console.WriteLine("Error executing query: " + ex.Message);
                    }
                    else
                    {
                        // we must throw exception we didn't handle
                        throw ex;
                    }
                }
                //select example(to come below)
                try
                {
                    SqlCommand cmdSelect = new SqlCommand("SELECT * FROM People", conn);
                    using (SqlDataReader reader = cmdSelect.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int id = (int)reader[0];
                            string name = (string)reader[1];
                            int age = (int)reader[2];
                            Console.WriteLine("Person {0}: {1} is {2} y/o", id, name, age);
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (ex is SqlException || ex is InvalidCastException)
                    {
                        Console.WriteLine("Error executing query: " + ex.Message);
                    }
                    else
                    {
                        // we must throw exception we didn't handle
                        throw ex;
                    }
                }
                // update below - ask user for record ID and new name, then update
                try
                {
                    Console.Write("Enter id: ");
                    int id = Int32.Parse(Console.ReadLine());
                    Console.Write("Enter a new Name: ");
                    string name = Console.ReadLine();
                    SqlCommand cmdUpdate = new SqlCommand(
                        "UPDATE People SET Name = @name where Id=@id ", conn);
                    cmdUpdate.Parameters.AddWithValue("Id", id);
                    cmdUpdate.Parameters.AddWithValue("Name", name);                
                    int rowsAffect = cmdUpdate.ExecuteNonQuery();
                    if (rowsAffect >= 1)
                    {
                        Console.WriteLine("Record updated successfully");
                    }
                    else
                    {
                        Console.WriteLine("No Record found");
                    }
                }
                catch (Exception ex)
                {
                    if (ex is SqlException || ex is InvalidCastException)
                    {
                        Console.WriteLine("Error executing update query: " + ex.Message);
                    }
                    else
                    {
                        // we must throw exception we didn't handle
                        throw ex;
                    }
                }
                // delete below - ask user for record ID and new name, then delete
                try
                {
                    Console.Write("Enter id: ");
                    int id = Int32.Parse(Console.ReadLine());
                    
                    SqlCommand cmdDelete = new SqlCommand(
                        "DELETE FROM People where Id=@id ", conn);

                    cmdDelete.Parameters.AddWithValue("Id", id);
                    int rowsAffect = cmdDelete.ExecuteNonQuery();
                    if (rowsAffect >= 1)
                    {
                        Console.WriteLine("Record Deleted successfully");
                    }
                    else
                    {
                        Console.WriteLine("No Record found");
                    }
                }
                catch (Exception ex)
                {
                    if (ex is SqlException || ex is InvalidCastException)
                    {
                        Console.WriteLine("Error executing delete query: " + ex.Message);
                    }
                    else
                    {
                        // we must throw exception we didn't handle
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is SqlException || ex is InvalidOperationException)
                {
                    Console.WriteLine("Error connecting to database: " + ex.Message);
                }
                else
                {
                    // we must throw exception we didn't handle
                    throw ex;
                }
            }
            finally
            {
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
            }
        }
    }
}
