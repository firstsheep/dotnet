﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz3Calc
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int Turn = 1; // 1 - value 1, 2 - Operate, 3 - Player 2, 4 - Result



        public MainWindow()
        {
            InitializeComponent();
        }


        private void Bt_Number_Click(object sender, RoutedEventArgs e)
        {
            Button bt_num = (Button)sender;
            if (Turn == 1)
            {

                lbFirstValue.Background = Brushes.Yellow;
                lbOperation_display.IsEnabled = false;
                tbSecondValue.IsEnabled = false;
                tbResult.IsEnabled = false;
                tbFirstValue.Text = tbFirstValue.Text + bt_num.Content;

            }
            else if (Turn == 3)
            {
                tbSecondValue.IsEnabled = true;
                lbOperation_display.IsEnabled = false;
                lbSecondValue.Background = Brushes.Yellow;
                lbOperation.Background = Brushes.White;
                tbSecondValue.Text = tbSecondValue.Text + bt_num.Content;
                // dis Enable operater
                btMulti.IsEnabled = false;
                btPlus.IsEnabled = false;
                btDivide.IsEnabled = false;
                btMinus.IsEnabled = false;
                // enable equal button
                btEqual.IsEnabled = true;
            }

        }

        private void Bt_Operate_Click(object sender, RoutedEventArgs e)
        {
            Turn = 2;
            lbOperation.Background = Brushes.Yellow;
            lbFirstValue.Background = Brushes.White;
            Button bt_ope = (Button)sender;
            if (!double.TryParse(tbFirstValue.Text, out double inputVal))
            {
                MessageBox.Show("Invalid input, must be numerical");
                tbFirstValue.Text = "";
                lbOperation.Background = Brushes.White;
                lbFirstValue.Background = Brushes.Yellow;
                return;
            }
            lbOperation_display.Content = bt_ope.Content;
            tbFirstValue.IsEnabled = false;
            tbSecondValue.IsEnabled = false;
            tbResult.IsEnabled = false;
            btEqual.IsEnabled = false;
            Turn = 3;


        }

        private void BtEqual_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Turn = 4;
                tbResult.IsEnabled = true;
                if (!double.TryParse(tbSecondValue.Text, out double inputVal))
                {
                    MessageBox.Show("Invalid input, must be numerical");
                    tbSecondValue.Text = "";
                    return;
                }
                lbSecondValue.Background = Brushes.White;
                lbResult.Background = Brushes.Yellow;
                double val1 = Convert.ToDouble(tbFirstValue.Text);
                double val2 = Convert.ToDouble(tbSecondValue.Text);
                switch (lbOperation_display.Content)
                {
                    case "*":
                        tbResult.Text = string.Format("{0:0.##########}", val1 * val2);
                        break;
                    case "+":
                        tbResult.Text = string.Format("{0:0.##########}", val1 + val2);
                        break;
                    case "-":
                        tbResult.Text = string.Format("{0:0.##########}", val1 - val2);
                        break;
                    case "/":
                        if (tbSecondValue.Text == "0")
                        {
                            MessageBox.Show("You can't devide by 0");
                            lbSecondValue.Background = Brushes.Yellow;
                            lbResult.Background = Brushes.White;
                            tbResult.IsEnabled = false;
                            return;
                        }
                        else
                        {
                            tbResult.Text = string.Format("{0:0.##########}", val1 / val2);
                        }
                        break;
                    default:
                        MessageBox.Show("Internal error");
                        break;
                }
                File.AppendAllText(@"..\..\..\result.txt", string.Format("{0} {1} {2} = {3}{4}", tbFirstValue.Text, lbOperation_display.Content,
                    tbSecondValue.Text, tbResult.Text,
                                Environment.NewLine));
                MessageBox.Show("Data saved");
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error accessing file: " + ex.Message);
            }

        }

        // CE -- Restart
        private void BtReset_Click(object sender, RoutedEventArgs e)
        {
            tbFirstValue.Text = "";
            tbSecondValue.Text = "";
            lbOperation_display.Content = "";
            tbResult.Text = "";

            Turn = 1;
            lbFirstValue.Background = Brushes.Yellow;
            lbSecondValue.Background = Brushes.White;
            lbOperation.Background = Brushes.White;
            lbResult.Background = Brushes.White;
            tbFirstValue.IsEnabled = true;
            lbOperation_display.IsEnabled = false;
            tbSecondValue.IsEnabled = false;
            tbResult.IsEnabled = false;
        }

        //FIXME
        private void BtChange_Click(object sender, RoutedEventArgs e)
        {
            try { 
            
                if (tbFirstValue.Text.StartsWith("-"))
                {
                    //It's negative now, so strip the `-` sign to make it positive
                    tbFirstValue.Text = tbFirstValue.Text.Substring(1);
                }
                else if (!string.IsNullOrEmpty(tbFirstValue.Text) && decimal.Parse(tbFirstValue.Text) != 0)
                {
                    //It's positive now, so prefix the value with the `-` sign to make it negative
                    tbFirstValue.Text = "-" + tbFirstValue.Text;
                }
           
                if (tbSecondValue.Text.StartsWith("-"))
                {
                    //It's negative now, so strip the `-` sign to make it positive
                    tbSecondValue.Text = tbSecondValue.Text.Substring(1);
                }
                else if (!string.IsNullOrEmpty(tbSecondValue.Text) && decimal.Parse(tbSecondValue.Text) != 0)
                {
                    //It's positive now, so prefix the value with the `-` sign to make it negative
                    tbSecondValue.Text = "-" + tbSecondValue.Text;
                }

            }catch(FormatException ex)
            {
                MessageBox.Show("You must input an integer. " + ex.Message);
            }
        }
    }

}

