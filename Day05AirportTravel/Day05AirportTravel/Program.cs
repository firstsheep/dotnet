﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace Day05AirportTravel
{
    class InvalidDataException : Exception
    {
        public InvalidDataException(string msg) : base(msg) { }
    }
    //completed 
    class Airport
    {
        
        public Airport(string code, string city, double latitude, double longitude)
        {
            Code = code;           
            City = city;
            Latitude = latitude;
            Longitude = longitude;
        }

        private string _code;
        public string Code
        {
            get { return _code; }
            set
            {
               // to check uppercase and 3
                if (!Regex.Match(value, "^[A-Z]{3}$").Success){
                    throw new InvalidDataException("Code must be 3 capital letters");
                }
            
                _code = value;
            }
        }
        private string _city;
                public string City
                {
                    get { return _city; }
                    set
                    {
                        if (value.Length < 2 || value.Length > 100)
                        {
                            throw new InvalidDataException("City must be 2-100 characters long");
                        }
                        _city = value;
                    }
                }
        private double _latitude;
        public double Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                if (value < -90 || value > 90)
                {
                    throw new InvalidDataException("latitude must be between -90 to 90");
                }
                _latitude = value;
            }

        }

        private double _longitude;
        public double Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                if (value < -180 || value > 180)
                {
                    throw new InvalidDataException("longitude must be between -180 - 180");
                }
                _longitude = value;
            }

        }

        public override string ToString()
        {
            return string.Format("{0} is located in {1}, latitude is {2} and longitutde is {3}", Code, City, Latitude, Longitude);
        }

    }
    class Program
    {
        const string FILE_PATH = @"..\..\..\airports.txt";

        static List<Airport> airportList = new List<Airport>();
        // completed
        static void AddAirport()
        {
            try
            {
                Console.WriteLine("Adding an airport.");
                Console.Write("Enter code: ");
                string code = Console.ReadLine();
                Console.Write("Enter city: ");
                string city = Console.ReadLine();
                Console.Write("Enter latitude: ");
                string latitudeStr = Console.ReadLine();
                double latitude;
                /*
                if (!int.TryParse(ageStr, out age))
                {
                    Console.WriteLine("Error: Age must be an integer");
                    return;
                }*/
                latitude = double.Parse(latitudeStr);
                Console.Write("Enter longitude: ");
                string longitudeStr = Console.ReadLine();
                double longitude;
                longitude = double.Parse(longitudeStr);
                Airport a = new Airport(code, city, latitude, longitude);
                airportList.Add(a);
            }
            catch (InvalidDataException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            
            catch (FormatException ex)
            {
                Console.WriteLine("Error: latitude and longitude must be a number");
            }
        }
        // completed
        static void ListAllAirportsInfo()
        {
            Console.WriteLine("Listing all airports");
            foreach (Airport a in airportList)
            {
                Console.WriteLine(a);
            }
        }

       

        public static double distance(double lat1, double lon1, double lat2, double lon2)
        {
            if ((lat1 == lat2) && (lon1 == lon2))
            {
                return 0;
            }
            else
            {
                double theta = lon1 - lon2;
                double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
                dist = Math.Acos(dist);
                dist = rad2deg(dist);
                dist = dist * 60 * 1.1515 * 1.609344;
                
                return (dist);
            }
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts decimal degrees to radians             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        public static double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts radians to decimal degrees             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        public static double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }

        
        // completed
        static void FindDistanceByCode()
        {
            Console.WriteLine("Enter a code of an airport:");
            string code1 = Console.ReadLine();
           
            Console.WriteLine("Enter the other code of an airport:");
            string code2 = Console.ReadLine();
            
             foreach (Airport a in airportList)
            {
                if (a.Code.Contains(code1))
                {
                    foreach (Airport b in airportList)
                    {
                        if (b.Code.Contains(code2))
                        {
                            Console.WriteLine("The distance is {0} km", distance(a.Latitude, a.Longitude, b.Latitude, b.Longitude)); 
                        }
                    }
                }
            }
        }

        static void FindNearestAirportByCode()
        {
            
            List<Double> distances = new List<Double>();
            Console.WriteLine("Enter a code of an airport:");
            string code = Console.ReadLine();
            foreach (Airport a in airportList)
            {
                if (a.Code.Contains(code))
                {
                    foreach (Airport b in airportList)
                    {
                        distances.Add(distance(a.Latitude, a.Longitude, b.Latitude, b.Longitude));
                    }
                }
            }
            double shortest_distance = Double.MaxValue;
            foreach (double d in distances)
            {
                if (d <= shortest_distance && d > 0)
                {
                    shortest_distance = d;
                    Console.WriteLine(d);
                }
            }
        }
        // completed
        static void ReadAllAirportsFromFile()
        {
            try
            {
                if (!File.Exists(FILE_PATH))
                {
                    return;
                }
                string[] contents = File.ReadAllLines(FILE_PATH);
                foreach (string line in contents)
                {
                    string[] data = line.Split(';');
                    if (data.Length != 4)
                    {
                        Console.WriteLine("Line corrupted, must have 4 fields exactly: " + line);
                        continue;
                    }
                    double latitude;
                    if (!double.TryParse(data[2], out latitude))
                    {
                        Console.WriteLine("Line corrupted, latitude must be a number: " + line);
                        continue;
                    }
                    double longitude;
                    if (!double.TryParse(data[3], out longitude))
                    {
                        Console.WriteLine("Line corrupted, longitude must be a number: " + line);
                        continue;
                    }
                    try
                    {
                        Airport a = new Airport(data[0],data[1], latitude, longitude);
                        airportList.Add(a);
                    }
                    catch (InvalidDataException ex)
                    {
                        Console.WriteLine("Line corrupted: " + line + "\nError:" + ex.Message);
                    }
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error: failed to read data from file: " + ex.Message);
            }
        }
        // completed
        static void SaveAllAirportsToFile()
        {
            try
            {
                List<string> contents = new List<string>();
                foreach (Airport a in airportList)
                {
                    contents.Add(String.Format("{0};{1};{2};{3}", a.Code, a.City, a.Latitude, a.Longitude));
                }
                File.WriteAllLines(FILE_PATH, contents);
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error: failed to save data to file: " + ex.Message);
            }
        }

        static int GetUserMenuChoice()
        {
            while (true)
            {
                Console.WriteLine("1.Show all airports (codes and city names)\n" +
                    "2.Find distance between two airports by codes\n" +
                    "3.Find the airports nearest to an airport given by code and display the distance\n" +
                    "4.Add a new airport to the list\n" +
                    "0.Exit\n" +
                    "Choice: ");
                string line = Console.ReadLine();
                int choice;
                if (int.TryParse(line, out choice))
                {
                    if (choice >= 0 && choice <= 4)
                    {
                        Console.WriteLine();
                        return choice;
                    }
                }
                Console.WriteLine("Invalid choice\n");
            }
        }

        static void Main(string[] args)
        {
            try
            {
                ReadAllAirportsFromFile();
                
                int choice;
                do
                {
                    choice = GetUserMenuChoice();
                    switch (choice)
                    {
                        case 1:
                            ListAllAirportsInfo();
                            break;
                        case 2:
                            FindDistanceByCode();
                            break;
                        case 3:
                            FindNearestAirportByCode();
                            break;
                        case 4:
                            AddAirport();
                            break;
                        case 0:
                            Console.WriteLine("Good bye.");
                            break;
                        default:
                            // yes, you must have a default handler - always
                            Console.WriteLine("Internal error: unknown choice.");
                            break;
                    }
                    Console.WriteLine();
                } while (choice != 0);
                SaveAllAirportsToFile();
            }
            finally
            {
                Console.WriteLine("press a key to end");
                Console.ReadKey();
            }
        }
    }
}
