﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01Hello
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.Write("Enter Your name: ");
                string yourName = Console.ReadLine();

                Console.Write("Enter Your age: ");
                int yourAge = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Hi {0}, you are {1} y / o.", yourName, yourAge);

                if (yourAge < 18 && yourAge > 0 )
                {
                    Console.WriteLine("You are not an adult yet.");
                    // Environment.Exit(1);
                    return;
                }
                if (yourAge < 0)
                {
                    Console.WriteLine("Your age is negative");
                    // Environment.Exit(1);
                    return;
                }

            }
            catch (FormatException ex)
            {
                Console.WriteLine("Error: Value entered must be an integer");
            }
            finally
            {
                Console.WriteLine("Press any key to end");
                Console.ReadKey();
            }

        }
    }
}
