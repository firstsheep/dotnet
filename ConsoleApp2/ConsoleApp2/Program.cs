﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03People
{
    class Person
    {
        /*
        public int Random
        {
            get {
                return new Random().Next(1,100);
            }
        } */

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2)
                {
                    throw new ArgumentOutOfRangeException("Name must be at least 2 characters long");
                }
                _name = value;
            }
        }

        // Age must be between 0-150 (both inclusive)
        private int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new ArgumentOutOfRangeException("Age must be between 0-150");
                }
                _age = value;
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // using an initalizer instead of a constructor
                Person p = new Person() { Name = "Jerry", Age = -1 };
                // p.Name = "M";
                p.Age = 77;
                Console.WriteLine("Person: {0} is {1} y/o", p.Name, p.Age);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("Exception: {0}", ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
            finally
            {
                Console.WriteLine("Press any key to finish");
                Console.ReadKey();
            }
        }
        
    }
}
