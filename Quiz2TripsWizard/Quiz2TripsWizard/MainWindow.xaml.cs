﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz2TripsWizard
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<TripWithCities> TripWithCitiesList = new List<TripWithCities>();
        public MainWindow()
        {
            InitializeComponent();
            lvTrip.ItemsSource = TripWithCitiesList;
            // 1. connect to database
            try
            {
                Globals.Db = new Database();
                // 2. load data in ListView
                ReloadList();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL Query\n", "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void ReloadList()
        {
            try
            {

                List<TripWithCities> list = Globals.Db.GetAllTripsWithCities();
                TripWithCitiesList.Clear();
                foreach (TripWithCities p in list)
                {
                    TripWithCitiesList.Add(p);
                }
                lvTrip.Items.Refresh();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Fatal Error: Unable to connect to database\n", "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
                Close(); // close the main window
            }
        }

        private void MenuItemExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void MenuItemExportSelected_Click(object sender, RoutedEventArgs e)
        {
            if (lvTrip.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select at least one TripWithCities", "TripWithCities List", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }


            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Trips|*.trips|All Files|*.*";
            saveFileDialog1.Title = "Save a trip File";
            saveFileDialog1.ShowDialog();


            if (saveFileDialog1.FileName != "")
            { //TODO: save data to file, semicolon separated
                List<TripWithCities> linesList = new List<TripWithCities>();
                try
                {

                    foreach (TripWithCities tripWithCities in lvTrip.SelectedItems)
                    {
                        linesList.Add(tripWithCities);
                    }

                    //  File.AppendAllLines(saveFileDialog1.FileName, linesList);
                    SaveDataToFile(saveFileDialog1.FileName, linesList);
                    MessageBox.Show("Data saved");
                    //   hasUnsavedData = false;

                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error saving to file: " + ex.Message);
                }
            }
        }
        void SaveDataToFile(string filePath, List<TripWithCities> dataList)
        {
            List<string> linesList = new List<string>();
            try
            {
                foreach (TripWithCities tripWithCities in dataList)
                {
                    linesList.Add(String.Format("{0};{1};{2};{3};{4};{5}", tripWithCities.TripId, tripWithCities.PersonName, tripWithCities.FromCityName, tripWithCities.ToCityName, tripWithCities.DepartureDate.ToString("yyyy-MM-dd"), tripWithCities.ReturnDate.ToString("yyyy-MM-dd")));

                }
                File.WriteAllLines(filePath, linesList);

               
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error saving to file: " + ex.Message, "TripWithCities List", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void MenuItemAddTrip_Click(object sender, RoutedEventArgs e)
        {
            // new window, instantiate
            AddTripDialog AddTripDialog = new AddTripDialog(this);
            if (AddTripDialog.ShowDialog() == true)
            {
                ReloadList();
            }
        }
    }
}
