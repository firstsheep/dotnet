﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2TripsWizard
{
    class Database
    {
        const String DbConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\ipd\Documents\dotnet\Quiz2TripsWizard\Quiz2TripsWizard.mdf;Integrated Security=True;Connect Timeout=30";
        private SqlConnection conn;

        public Database()
        {
            conn = new SqlConnection(DbConnectionString);
            conn.Open();
        }
        public List<TripWithCities> GetAllTripsWithCities() {
            List<TripWithCities> list = new List<TripWithCities>();
            SqlCommand cmdSelect = new SqlCommand("SELECT T.Id TripId, T.PersonName PersonName, C1.CityName FromCityName, C2.CityName ToCityName, T.DepartureDate DepartureDate, T.ReturnDate ReturnDate FROM Cities as C1, Trips as T, Cities as C2 WHERE C1.Id = T.FromCityId AND C2.Id = T.ToCityId", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int tripId = (int)reader["TripId"];
                    string personName = (string)reader[1];
                    string fromCityName = (string)reader[2];
                    string toCityName = (string)reader[3];
                    DateTime departureDate = (DateTime)reader[4];
                    DateTime returnDate = (DateTime)reader[5];
                
                    list.Add(new TripWithCities() { TripId = tripId, PersonName = personName, FromCityName = fromCityName, ToCityName = toCityName, DepartureDate = departureDate, ReturnDate = returnDate });
                }
            }
            return list;
        }
        public List<City> GetAllCities() {
            List<City> list = new List<City>();
            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM Cities", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    string cityName = (string)reader["CityName"];
                    double latitude = (double)reader["Latitude"];
                    double longitude = (double)reader["Longitude"];

                    list.Add(new City() { Id = id, CityName = cityName, Latitude = latitude, Longitude = latitude});
                }
            }
            return list;
        }
        public void AddTrip(Trip trip) {
           
        }
    }
}
