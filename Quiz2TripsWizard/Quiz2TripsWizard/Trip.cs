﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2TripsWizard
{
    class Trip
    {
        public int Id { get; set; }
        public string PersonName { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public int FromCityId { get; set; }
        public int ToCityId { get; set; }
    }
}
