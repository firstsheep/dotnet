﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz2TripsWizard
{
    /// <summary>
    /// Interaction logic for InputNameDateDialog.xaml
    /// </summary>
    public partial class InputNameDateDialog : Window
    {
       
        public InputNameDateDialog(Window owner, String fromCity, String toCity)
        {
            InitializeComponent();
            Owner = owner;
            lbFromCity.Content = fromCity;
            lbToCity.Content = toCity;

            
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
       

    }
}
