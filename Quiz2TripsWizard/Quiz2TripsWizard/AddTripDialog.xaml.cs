﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz2TripsWizard
{
    /// <summary>
    /// Interaction logic for AddTripDialog.xaml
    /// </summary>
    public partial class AddTripDialog : Window
    {
       
        List<String> CityNameList = new List<String>();
        
        public AddTripDialog(Window owner)
        {
            InitializeComponent();
            Owner = owner;
            lvFromCity.ItemsSource = CityNameList;
            lvToCity.ItemsSource = CityNameList;
            // 1. connect to database
            try
            {
                Globals.Db = new Database();
                // 2. load data in ListView
                ReloadList();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL Query\n", "Trip Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void ReloadList()
        {
            try
            {

                List<City> list = Globals.Db.GetAllCities();
                CityNameList.Clear();
                
                foreach (City c in list)
                {
                    CityNameList.Add(c.ToString());
                }
                lvFromCity.Items.Refresh();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Fatal Error: Unable to connect to database\n", "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
                Close(); // close the main window
            }
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            // lvFromCity.SelectedIndex = -1;
            // lvToCity.SelectedIndex = -1;
            Close();
        }

        private void LvToCity_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // FIXME
            if (lvFromCity.SelectedValue == null)    // not selected anything
            {
                MessageBox.Show("Please first select a city you are going from");
                lvFromCity.SelectedIndex = -1;
            }else if (lvFromCity.SelectedValue == lvToCity.SelectedValue)
            {
                MessageBox.Show("You can't select the same city");
            }
            else
            {
                btNext.IsEnabled = true;
                
            }
        }

        private void BtNext_Click(object sender, RoutedEventArgs e)
        {
            // new window, instantiate
            String FromCitySelected = lvFromCity.SelectedItem as String;
            String ToCitySelected = lvToCity.SelectedItem as String;
            InputNameDateDialog InputNameDateDialog = new InputNameDateDialog(this,FromCitySelected,ToCitySelected);
            if (InputNameDateDialog.ShowDialog() == true)
            {
                ReloadList();
                                
            }
        }
    }
}
