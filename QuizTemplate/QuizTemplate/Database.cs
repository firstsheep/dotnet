﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizTemplate
{
    public class Database
    {
        // Set DbConnectionString from DB Property
        const string DbConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Jeonghoon\Documents\labs\DotNet-II\JhDB.mdf;Integrated Security=True;Connect Timeout=30";

        private SqlConnection conn;

        // DB Constructor
        public Database()
        {
            conn = new SqlConnection(DbConnectionString);
            conn.Open();
        }

        //
        // Using SQL ORDER BY, it sorted by parameter
        // if you don't use sql sorting, delete this
        public List<Item> GetAllItems(string sortBy = "Id")
        {
            List<Item> list = new List<Item>();
            // ORDER BY
            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM Todos ORDER BY " + sortBy, conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader[0];
                    string task = (string)reader[1];
                    DateTime dueDate = (DateTime)reader[2];
                    EItemStatus isDone = (EItemStatus)Enum.Parse(typeof(EItemStatus), (string)reader[3]);
                    list.Add(new Item() { Id = id, Task = task, DueDate = dueDate, IsDone = isDone });
                }
            }
            return list;
        }

        //
        // Using LINQ, Not use ORDER BY..
        // Return all the list
        // if you use sql sorting, delete this method.
        public List<Item> GetAllItems()
        {
            List<Item> list = new List<Item>();
            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM Todos", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader[0];
                    string task = (string)reader[1];
                    DateTime dueDate = (DateTime)reader[2];
                    EItemStatus isDone = (EItemStatus)Enum.Parse(typeof(EItemStatus), (string)reader[3]);
                    list.Add(new Item() { Id = id, Task = task, DueDate = dueDate, IsDone = isDone });
                }
            }
            return list;
        }

        public int AddItem(Item item)
        {
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO Todos (Task, DueDate, IsDone) OUTPUT INSERTED.ID VALUES (@Task, @DueDate, @IsDone)", conn);
            cmdInsert.Parameters.AddWithValue("Task", item.Task);
            cmdInsert.Parameters.AddWithValue("DueDate", item.DueDate);
            cmdInsert.Parameters.AddWithValue("IsDone", item.IsDone.ToString());    // Convert to string

            int insertId = (int)cmdInsert.ExecuteScalar();
            item.Id = insertId; // we may choose to do it
            return insertId;
        }

        public bool UpdateItem(Item item)
        {
            SqlCommand cmdUpdate = new SqlCommand("UPDATE Todos SET Task=@Task, DueDate=@DueDate, IsDone=@IsDone WHERE Id=@Id", conn);
            cmdUpdate.Parameters.AddWithValue("Id", item.Id);
            cmdUpdate.Parameters.AddWithValue("Task", item.Task);
            cmdUpdate.Parameters.AddWithValue("DueDate", item.DueDate);
            cmdUpdate.Parameters.AddWithValue("IsDone", item.IsDone.ToString());    // Convert to string
            int rowsAffected = cmdUpdate.ExecuteNonQuery();

            return rowsAffected > 0;
        }

        public bool DeleteItem(int id)
        {
            SqlCommand cmdDelete = new SqlCommand("DELETE FROM Todos WHERE Id=@Id", conn);
            cmdDelete.Parameters.AddWithValue("Id", id);
            int rowsAffected = cmdDelete.ExecuteNonQuery();

            return rowsAffected > 0;
        }

    }
}
