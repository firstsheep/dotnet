﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QuizTemplate
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        enum EItemSortOrder { Id, Task, DueDate, IsDone };
        EItemSortOrder ItemSortOrder = EItemSortOrder.Id;       // set default sort column

        // Keep the information to display
        List<Item> ItemsList = new List<Item>();

        public MainWindow()
        {
            InitializeComponent();
            // Bind view with data
            lvItems.ItemsSource = ItemsList;

            // 1. Connect to datase
            try
            {
                Globals.Db = new Database();
                // 2. Load data in ListView
                ReloadList();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Fatal error: unable to connect to database\n" + ex.Message,
                    "Items Database", MessageBoxButton.OK, MessageBoxImage.Error);
                Close(); // close the main window, terminate the program
            }
        }

        // Reload Data from database
        // If search keyword is set, search and then display
        private void ReloadList()
        {
            try
            {
                // Using SQL Server sorting
                // List<Item> list = Globals.Db.GetAllItems(TaskSortOrder.ToString());

                // Using LINQ
                List<Item> list = Globals.Db.GetAllItems();
                switch (ItemSortOrder)
                {
                    case EItemSortOrder.Id:
                        // Functional 
                        // list = list.OrderBy(t => t.Id).ToList<Item>();

                        // both are same
                        // this is query-style
                        var query = from item in list orderby item.Id select item;
                        list = query.ToList();
                        break;
                    case EItemSortOrder.Task:
                        break;
                    case EItemSortOrder.DueDate:
                        break;
                    case EItemSortOrder.IsDone:
                        break;
                    default:
                        MessageBox.Show("Internal error: unknown sort column", "Items Database", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                }

                //////////////////////////////////////////////////////////
                // this part is for search, delete this code if we don't need to search 
                //
                // if Search string is set
                string searchStr = tbSearch.Text;
                if (searchStr != "")
                {
                    var query = from item in list where item.Task.Contains(searchStr) select item;
                    list = query.ToList();
                }
                //////////////////////////////////////////////////////////

                // Clear old ItemsList
                ItemsList.Clear();
                // Add new list to ItemsList
                ItemsList.AddRange(list.ToList());
                // AddRange() is same as below
                /*
                foreach (Item p in list)
                {
                    ItemsList.Add(p);
                }
                */
                lvItems.Items.Refresh();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL query:\n" + ex.Message, "Items Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void AddItem_MenuClick(object sender, RoutedEventArgs e)
        {
            AddEditDialog addEditDialog = new AddEditDialog(this);
            if (addEditDialog.ShowDialog() == true)
            {
                ReloadList();
            }
        }

        private void DeleteItem_ContexMenuClick(object sender, RoutedEventArgs e)
        {
            Item currItem = lvItems.SelectedItem as Item;
            if (currItem == null) return;
            //
            MessageBoxResult result = MessageBox.Show(this, "Are you sure you want to delete:\n" +
                currItem, "Confirm delete", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            //
            if (result == MessageBoxResult.OK)
            {
                try
                {
                    Globals.Db.DeleteItem(currItem.Id);
                    ReloadList();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("Error executing SQL query:\n" + ex.Message,
                        "Items Database", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void EditItem_ContexMenuClick(object sender, RoutedEventArgs e)
        {
            LvItems_MouseDoubleClick(sender, null); // "redirect" the event
        }

        private void LvItems_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Item currItem = lvItems.SelectedItem as Item;
            if (currItem == null) return;

            AddEditDialog addEditDialog = new AddEditDialog(this, currItem);
            if (addEditDialog.ShowDialog() == true)
            {
                ReloadList();
            }
        }

        private void LvItems_HeaderClick(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader headerClicked = e.OriginalSource as GridViewColumnHeader;

            if (headerClicked == null) { return; }

            if (headerClicked.Role != GridViewColumnHeaderRole.Padding)     // check clicked header is not padding area (padding area means last empty header area)
            {
                switch (headerClicked.Tag)
                {
                    case "Id":
                        ItemSortOrder = EItemSortOrder.Id;
                        break;
                    case "Task":
                        ItemSortOrder = EItemSortOrder.Task;
                        break;
                    case "DueDate":
                        ItemSortOrder = EItemSortOrder.DueDate;
                        break;
                    case "Status":
                        ItemSortOrder = EItemSortOrder.IsDone;
                        break;
                    default:
                        MessageBox.Show("Internal error: unknown sort column",
                            "Items Database", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                }
            }
            ReloadList();
        }

        private void TbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            ReloadList();
        }
    }
}
