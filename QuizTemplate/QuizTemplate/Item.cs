﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizTemplate
{
    public class Item
    {
        public int Id { get; set; }
        public string Task { get; set; }
        public DateTime DueDate { get; set; }
        public EItemStatus IsDone { get; set; }
    }

    // Define Enum type
    public enum EItemStatus { Pending = 2, Done = 1 }
}
