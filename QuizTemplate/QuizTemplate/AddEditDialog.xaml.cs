﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuizTemplate
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        Item EditedItem;

        public AddEditDialog(Window owner, Item editedItem = null)
        {
            InitializeComponent();
            Owner = owner;
            EditedItem = editedItem;
            if (EditedItem != null)
            {
                Title = "Update Item";
                btAddEdit.Content = "Update";
                // in case of update
                // update display
                lblId.Content = EditedItem.Id;
            }
        }

        private void ButtonAddEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (EditedItem == null)
                {
                    // TODO: Database Add
                    // Globals.Db.AddTodo(Todo);
                }
                else
                {
                    // TODO: Database Update
                    // Globals.Db.UpdateItem(EditedItem);
                }
                DialogResult = true;
            }
            catch (Exception ex)
            {
                if (ex is SqlException)
                {
                    MessageBox.Show("Error executing SQL query:\n" + ex.Message, "Todo Database", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    throw ex;
                }
            }
        }
    }
}
