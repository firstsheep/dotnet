﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day12AirportsList
{
    class InvalidDataException : Exception
    {
        public InvalidDataException(string message) : base(message) { }
    }

    class Airport
    {
        public Airport() { }

        // Constructor to load from file
        public Airport(string line)
        {
            // BBB;222;1;1;1111
            string[] data = line.Split(';');

            if (data.Length != 5)
            {
                throw new InvalidDataException("Invalid Data");
            }
            // FIXME: need to implement error handling
            Code = data[0]; // BBB
            City = data[1]; // 222

            double.TryParse(data[2], out double latitude);
            Latitude = latitude;
            double.TryParse(data[3], out double longitude);
            Longitude = longitude;
            int.TryParse(data[4], out int elevationMeters);
            ElevationMeters = elevationMeters;
        }

        public Airport(string code, string city, double latitude, double logitude, int elevationMeters)
        {
            Code = code;
            City = city;
            Latitude = latitude;
            Longitude = logitude;
            ElevationMeters = elevationMeters;
        }

        private string _code;
        public string Code
        {
            get { return _code; }
            set
            {
                if (!Regex.IsMatch(value, @"^[A-Z]{3}$"))
                {
                    throw new InvalidDataException("[Error] Code should be 3 upper-case letters. : " + value);
                }
                _code = value;
            }
        }

        private string _city;
        public string City
        {
            get { return _city; }
            set
            {
                if (value == "")
                {
                    throw new InvalidDataException("[Error] City must not be empty.");
                }
                _city = value;
            }
        }

        private double _latitude;
        public double Latitude
        {
            get { return _latitude; }
            set
            {
                if ((value < -90.0) || (90.0 < value))
                {
                    throw new InvalidDataException("[Error] Range of latitudes should be from -90 to 90. : " + value);
                }
                _latitude = value;
            }
        }

        private double _longitude;
        public double Longitude
        {
            get { return _longitude; }
            set
            {
                if ((value < -180.0) || (180.0 < value))
                {
                    throw new InvalidDataException("[Error] Range of longitudes should be from -180 to 180. : " + value);
                }
                _longitude = value;
            }
        }

        public int ElevationMeters;

        public override string ToString()
        {
            return string.Format("{0}:{1} Airport - Latitude: {2}, Longitude: {3}, ElevationMeters: {4}", Code, City, Latitude, Longitude, ElevationMeters);
        }

        // to save data into file
        public string ToSaveFormat()
        {
            return string.Format("{0};{1};{2};{3};{4}", Code, City, Latitude, Longitude, ElevationMeters);
        }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const string FILE_NAME = @"airport.txt";
        List<Airport> airportList = new List<Airport>();

        public void LoadDataFromFile()
        {
            try
            {
                string[] dataList = File.ReadAllLines(FILE_NAME);
                foreach (string line in dataList)
                {
                    airportList.Add(new Airport(line));
                }
            }
            catch (IOException ex)
            {
                // FIXME: implement to handle error
            }
        }

        public MainWindow()
        {
            // Load data from default file. 
            LoadDataFromFile();

            InitializeComponent();
            // connect data to view
            lvAirport.ItemsSource = airportList;
        }

        private void ButtonAddAndUpdateAirport_Click(object sender, RoutedEventArgs e)
        {
            string code = tbCode.Text;
            string city = tbCity.Text;
            string latitudeStr = tbLatitude.Text;
            string longitudeStr = tbLongitude.Text;
            string elevationMeterStr = tbElevationMeters.Text;

            // TODO: validation check for data
            // 1. code is valid or not
            // 2. city length and (; not allowed to include) Regex.IsMatch(city, @"^[^;]{1,100}$")

            if (!double.TryParse(latitudeStr, out double latitude))
            {
                // FIXME: Need to implement error handling
            }
            if (!double.TryParse(longitudeStr, out double longitude))
            {
                // FIXME: Need to implement error handling
            }
            if (!int.TryParse(elevationMeterStr, out int elevationMeters))
            {
                // FIXME: Need to implement error handling
            }

            // Button senderButton = sender as Button;

            if ((sender as Button).Name == "btUpdate")
            {
                // Update
                Airport selectedAirport = lvAirport.SelectedItem as Airport;

                if (selectedAirport == null)
                {
                    // handle error
                    return;
                }

                selectedAirport.Code = code;
                selectedAirport.City = city;
                selectedAirport.Latitude = latitude;
                selectedAirport.Longitude = longitude;
                selectedAirport.ElevationMeters = elevationMeters;
            }
            else
            {
                // new object (ADD)
                Airport airport = new Airport();

                airport.Code = code;
                airport.City = city;
                airport.Latitude = latitude;
                airport.Longitude = longitude;
                airport.ElevationMeters = elevationMeters;

                airportList.Add(airport);
            }

            // update UI
            lvAirport.Items.Refresh();
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            try
            {
                // Create string list to save data
                List<string> dataList = new List<string>();
                foreach (Airport airport in airportList)
                {
                    dataList.Add(airport.ToSaveFormat());
                }
                // save string list to file
                File.WriteAllLines(FILE_NAME, dataList);
            } catch (IOException ex)
            {
                // FIXME: need to handle exception
            }
        }

        private void LvAirport_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Airport selectedAirport = lvAirport.SelectedItem as Airport;

            if (selectedAirport == null)
            {
                return;
            }

            tbCode.Text = selectedAirport.Code;
            tbCity.Text = selectedAirport.City;
            tbLatitude.Text = string.Format("{0:0.00}", selectedAirport.Latitude);
            tbLongitude.Text = selectedAirport.Longitude.ToString();
            tbElevationMeters.Text = selectedAirport.ElevationMeters.ToString();
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            Airport selectedAirport = lvAirport.SelectedItem as Airport;

            if (selectedAirport == null)
            {
                // handle error
                return;
            }
            // need to implement dialog
            airportList.Remove(selectedAirport);

            lvAirport.Items.Refresh();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            /* for notepad
            switch(MessageBox.Show("Are you sure to exit?", "Airport", MessageBoxButton.YesNoCancel, MessageBoxImage.Information, MessageBoxResult.Cancel))
            {
                case MessageBoxResult.Yes:
                    // display save dialog
                    // SaveFileDialog saveDlg = new SaveFileDialog();
                    // saveDlg.ShowDialog() ;
                    break;
                case MessageBoxResult.No:
                    // close program
                    break;
                case MessageBoxResult.Cancel:
                    e.Cancel = true;
                    break;
                default:
                    MessageBox.Show("Internal error");
                    break;
            }
            */
        }

        private void SortLatitude_MenuClick(object sender, RoutedEventArgs e)
        {
            airportList = airportList.OrderByDescending(airport => airport.Latitude).ToList();

            //    var query = from airport in airportList orderby airport.Latitude descending select airport;
            //  airportList = query.ToList();

            lvAirport.ItemsSource = airportList;            
        }
    }
}
