﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01Find
{
    class Program
    {
        static string EnterValues()
        {
            Console.Write("Enter a float: ");
            string userValue = Console.ReadLine();
            return userValue;
        }
        static double FindSmallestNumber(List<double> numbers)
        {
            numbers = new List<double>();
            double minNumber = numbers.Min();
            return minNumber;
        }
        static double FindSumOfAllNumbers()
        {

        }
        static double FindAverage()
        {

        }
        static double FindStandardDeviation()
        {

        }
        static void Main(string[] args)
        {
          
 
            List<double> userFloats = new List<double>();

            for (int i = 0; i < 5; i++)
            {
                EnterValues();
                double userFloat;
                if (double.TryParse(EnterValues(), out userFloat))
                {
                    userFloats.Add(userFloat);
                }
            }
            userFloats.ForEach(i => Console.Write("{0}\t", i));
            Console.WriteLine("{0}\t", FindSmallestNumber(userFloats));


            Console.WriteLine("Press any key to end");
            Console.ReadKey();

        }
    }
}
