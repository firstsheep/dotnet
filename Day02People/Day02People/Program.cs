﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02People
{
    class Program
    {
        static String[] namesArray = new String[4];
        static int[] agesArray = new int[4];
        const int length = 4;
        static void EnterValues()
        {
            for (int i = 0; i < length; i++)
            {
                Console.Write("Enter name of person #" + (i+1) + ": ");
                string line1 = Console.ReadLine();
                Console.Write("Enter age of person #" + (i + 1) + ": ");
                int line2 = Convert.ToInt32(Console.ReadLine());

                if (String.IsNullOrEmpty(line1))
                {
                    Console.WriteLine("Invalid input. Exiting.");
                    return;
                }
                namesArray[i] = line1;
                agesArray[i] = line2;

            }
            
        }
        static void FindYoungest()
        {
            // int smallest = int.MaxValue;
            int minAge = agesArray[0];
            foreach (int age in agesArray)
            {
                if (age < minAge)
                {
                    minAge = age;
                }
            }
            int pos = Array.IndexOf(agesArray, minAge);
            Console.WriteLine("Youngest person is " + minAge + " and their name is " + namesArray[pos]);
        }
        static void Main(string[] args)
        {
            EnterValues();
            FindYoungest();
            Console.ReadKey();

        }
    }
}
