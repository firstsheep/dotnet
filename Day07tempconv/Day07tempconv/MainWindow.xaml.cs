﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07tempconv
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        private void Input_tbName_TextChanged(object sender, TextChangedEventArgs e)
        {
            calculate();
        }



        private void Rb_change_Click(object sender, RoutedEventArgs e)
        {
            calculate();
        }
        private void calculate()
        {
            // step 1: read input, validate, etc
            string input_valueStr = input_tbName.Text;
            if (input_valueStr == "")
            {
                return;
            }
            double input_value;
            if (!double.TryParse(input_valueStr, out input_value))
            {
                MessageBox.Show("Invalid Input");
                input_valueStr = "";
                return;
            }
            // step 2: convert from input to celsius
            double celsius = 0;
            if (rbIC.IsChecked == true)
            {
                celsius = input_value;
            }
            else if (rbIF.IsChecked == true)
            {
                celsius = (input_value - 32) * 5 / 9;
            }
            else if (rbIK.IsChecked == true)
            {
                celsius = input_value - 273.15;
            }
            else
            {
                MessageBox.Show("Internal error. Unknown input scale.");
                return;
            }

            // step 3 : convert from celsius to output
            double output_value = 0;
            string suffix = "";
            if (rbOC.IsChecked == true)
            {
                output_value = celsius;
                suffix = "C";
            }
            else if (rbOF.IsChecked == true)
            {
                output_value = celsius * 9 / 5 + 32;
                suffix = "F";
            }
            else if (rbOK.IsChecked == true)
            {
                output_value = celsius + 273.15;
                suffix = "K";
            }

            // step 4 
            //   output_tbName.Text = output_value.ToString("0.00") + suffix;
            output_tbName.Text = string.Format("{0:0.##°}{1}", output_value, suffix);
            //atl +248 or alt+0176
        }
    }
}