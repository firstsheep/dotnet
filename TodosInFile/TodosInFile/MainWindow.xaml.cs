﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TodosInFile
{
    public class TodoItem
    {
        public TodoItem(string ToDataLine)
        {
            //de-serialization
            string[] data = ToDataLine.Split(',');
            if (data.Length != 3)
            {
                throw new InvalidDataException("Data line must be composed of 3 items exactly");
            }
            //TODO: add length verification
            Task = data[0];
            DueDate = DateTime.ParseExact(data[1], "yyyy-MM-dd", null);
            //TODO: CHECK
            IsDone = data[2] == "done";
        }
        public TodoItem() { }
        public String Task;
        public DateTime DueDate;
        public bool IsDone;


        public override string ToString()
        {
            return String.Format("{0}, {1}, {2}", Task, DueDate.ToShortDateString(), IsDone ? "is done" : "pending");
        }
        public string ToDataLine()
        {//serialization
            return String.Format("{0};{1};{2}", Task, DueDate.ToString("yyyy-MM-dd"), IsDone ? "is done" : "pending");
        }
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public const string FILE_NAME = @"..\..\..\todolist.dat";
        List<TodoItem> TodoList = new List<TodoItem>();
        bool hasUnsavedData = false;
        public MainWindow()
        {
            InitializeComponent();
            //  TodoList.Add(new TodoItem() { Task = "Buy milk", DueDate = DateTime.ParseExact("2019-10-27", "yyyy-mm-dd", null), IsDone = false });
            //  TodoList.Add(new TodoItem() { Task = "Write homework", DueDate = new DateTime(), IsDone = true });
            //    LoadFromFile(FILE_NAME);
            LoadFromFile(FILE_NAME);
            lvTodos.ItemsSource = TodoList;
        }

        private void FileExportSelected_MenuClick(object sender, RoutedEventArgs e)
        {
            if (lvTodos.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select at least one thing", "Todo List", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }


            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Text files|*.txt|All Files|*.*";
            saveFileDialog1.Title = "Save an data File";
            saveFileDialog1.ShowDialog();

           
            if (saveFileDialog1.FileName != "")
            { //TODO: save data to file, semicolon separated
                try { 
                List<TodoItem> linesList = new List<TodoItem>();
                foreach (TodoItem todo in lvTodos.SelectedItems)
                {
                        //linesList.Add(string.Format("{0};{1};{2}{3}", todo.Task, todo.DueDate, todo.IsDone,
                        //                           Environment.NewLine));
                       linesList.Add(todo);
                }

                    //  File.AppendAllLines(saveFileDialog1.FileName, linesList);
                    SaveToFile(saveFileDialog1.FileName, linesList);
                    MessageBox.Show("Data saved");
                    hasUnsavedData = false;

                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error saving to file: " + ex.Message);
                }
            }
            lbStatusBar.Text = "Export data to file";

        }

        private void FileImport_MenuClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog theDialog = new OpenFileDialog();
            theDialog.Title = "Import todo items";
            theDialog.Filter = "Todo Lists|*.todos|All files|*.*";
            //  theDialog.InitialDirectory = @"C:\";
            //  theDialog.FileName = "Document"; // Default file name
            // theDialog.DefaultExt = ".txt"; // Default file extension
            if (theDialog.ShowDialog() == true)
            {
                try { 
                LoadFromFile(theDialog.FileName);
                lvTodos.Items.Refresh();
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error loading data from file: " + ex.Message, "Todo List", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Would you like to EXIT", "Todo list", MessageBoxButton.OKCancel);
            try
            {
                switch (result)
                {
                    case MessageBoxResult.OK:
                        Close();
                        break;
                    case MessageBoxResult.Cancel:
                        MessageBox.Show("Nevermind then...", "My App");
                        break;

                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error accessing file: " + ex.Message);
            }
            lbStatusBar.Text = "want to exit";
        }
        public void LoadFromFile(string fileName)
        {
            try
            {

                string[] filelines = File.ReadAllLines(fileName);

                foreach (string line in filelines)
                {
                    try
                    {
                        TodoList.Add(new TodoItem(line));
                    }
                    catch (InvalidDataException ex)
                    {
                        // FIXME: Collect errors and only show a single error message box after the loop
                        MessageBox.Show("Error reading line from file: " + ex.Message, "Todo List", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }

            }
            catch (FileNotFoundException ex) { } // ignore
            catch (IOException ex)
            {
                MessageBox.Show("Error saving to file: " + ex.Message, "Todo List", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        public void SaveToFile(string fileName, List<TodoItem> itemsList)
        {
            List<string> linesList = new List<string>();
            try
            {
                foreach (TodoItem todo in itemsList)
                {
                    // linesList.Add(string.Format("{0};{1};{2}{3}", todo.Task, todo.DueDate, todo.IsDone,
                    //                             Environment.NewLine));
                    linesList.Add(todo.ToDataLine());

                }
                File.WriteAllLines(fileName, linesList);
                lbStatusBar.Text = string.Format("Wrote {0} todo items to file {1}", linesList.Count, fileName);
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error saving to file: " + ex.Message, "Todo List", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Button_AddUpdateTask_Click(object sender, RoutedEventArgs e)
        {
            if (!Regex.IsMatch(tbTask.Text, @"^[^;]{1,100}$"))
            {
                MessageBox.Show("task input invalid, must be 1-100 characters, no semicolons");
                return;
            }
            DateTime date = dpDate.SelectedDate.Value;
            if (date == null)
            {
                MessageBox.Show("Please choose a date", "Todo list", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }
            /*  bool status = false;
              if (rbPending.IsChecked == true)
              {
                  status = false;
              }
              else if (rbDone.IsChecked == true)
              {
                  status = true;
              }
              else
              {
                  // WTF internal error
                  MessageBox.Show("internal errors");
                  return;
              }
              */
            bool isDone = rbDone.IsChecked == true;

            Button senderButton = sender as Button;
            if (senderButton.Name == "btUpdateTask")
            {
                TodoItem item = lvTodos.SelectedItem as TodoItem;
                item.Task = tbTask.Text;
                item.DueDate = date;
                item.IsDone = isDone;
                lbStatusBar.Text = "Update 1 task to the list";
            }
            else
            {
                TodoItem item = new TodoItem() { Task = tbTask.Text, DueDate = date, IsDone = isDone };
                TodoList.Add(item);
                
                hasUnsavedData = true;
                // clean up GUI inputs
                tbTask.Text = "";
                dpDate.SelectedDate = DateTime.Today;
                rbPending.IsChecked = false;
                rbDone.IsChecked = false;
                lbStatusBar.Text = "Add 1 task to the list";
            }
            lvTodos.Items.Refresh();
            
        }

        private void SortByTask_MenuClick(object sender, RoutedEventArgs e)
        {
            TodoList = TodoList.OrderBy(TodoItem => TodoItem.Task).ToList<TodoItem>();
            lvTodos.ItemsSource = TodoList;

        }

        private void SortByDueDate_MenuClick(object sender, RoutedEventArgs e)
        {
            TodoList = TodoList.OrderBy(TodoItem => TodoItem.DueDate).ToList<TodoItem>();
            lvTodos.ItemsSource = TodoList;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            
                SaveToFile(FILE_NAME, TodoList);
           
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            TodoItem item = lvTodos.SelectedItem as TodoItem;
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete:\n" + item.ToString(), "Todo list", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if(result == MessageBoxResult.OK)
            {
                TodoList.Remove(item);
                lvTodos.Items.Refresh();
            }

        }

        private void LvTodos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TodoItem item = lvTodos.SelectedItem as TodoItem;
            if(item == null)
            {
                // nothing selected
                btUpdateTask.IsEnabled = false;
                btDeleteTask.IsEnabled = false;
                return;
            }
            btUpdateTask.IsEnabled = true;
            btDeleteTask.IsEnabled = true;
            //
            tbTask.Text = item.Task;
            dpDate.SelectedDate = item.DueDate;
            rbDone.IsChecked = item.IsDone;
            rbPending.IsChecked = !item.IsDone; 

        }
    }

        
    }
