﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02NamesAgain
{
    class Program
    {
        static String[] nameList = new String[5];
        const int length = 5;
        static void EnterValues()
        {
            for (int i = 0; i < length; i++)
            {
                Console.Write("Enter a name: ");
                string line = Console.ReadLine();

                if (String.IsNullOrEmpty(line))
                {
                    Console.WriteLine("Invalid input. Exiting.");
                    return;
                }
                nameList[i] = line;

            }
            foreach (string name in nameList)
            {
                Console.WriteLine(name);
            }
        }
        static void findNames()
        {
            Console.Write("Enter a search: ");
            string search = Console.ReadLine();
            foreach (string name in nameList)
            {
                if (name.Contains(search))
                {
                    int pos = Array.IndexOf(nameList, name);
                    if (pos > -1)
                    {
                        Console.WriteLine("Matching name: {0}", name);
                    }
                }
            }
        }
        static void findLongestNames()
        {
            string maxValue = nameList[0];
            foreach (string name in nameList)
            {
                if (name.Length > maxValue.Length)
                {
                    maxValue = name;
                }
            }

            Console.WriteLine("Longest name is: {0}", maxValue);
        }
            static void Main(string[] args)
        {
            try
            {
                EnterValues();               
                findNames();
                findLongestNames();
            }
            finally
            {
                Console.WriteLine("Press any key to end");
                Console.ReadKey();
            }
        }
    }
}
