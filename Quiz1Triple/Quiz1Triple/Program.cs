﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1Triple
{
    class Program
    {
        // part 1 
        public static double LetterGradetoNumber(String grade)
        {
            switch (grade)
            {
                case "A":
                    grade = "4";
                    return Convert.ToDouble(grade);
                    // return 4.0;
                //    break;
                case "A-":
                    grade = "3.7";
                    return Convert.ToDouble(grade);
                //    break;
                case "B+":
                    grade = "3.3";
                    return Convert.ToDouble(grade);
                 //   break;
                case "B":
                    grade = "3";
                    return Convert.ToDouble(grade);
                 //   break;
                case "B-":
                    grade = "2.7";
                    return Convert.ToDouble(grade);
                //    break;
                case "C+":
                    grade = "2.3";
                    return Convert.ToDouble(grade);
                //    break;
                case "C":
                    grade = "2";
                    return Convert.ToDouble(grade);
                //    break;
                case "D":
                    grade = "1";
                    return Convert.ToDouble(grade);
                //    break;
                case "F":
                    grade = "0";
                    return Convert.ToDouble(grade);
                //    break;
                default:
                    throw new InvalidProgramException();
              //      grade = "0";
                //    return Convert.ToDouble(grade);
               //     break;
            }
        }
        static void GPACalculator()
        {
            List<string> gradeList = new List<string>();
            Console.WriteLine("Enter letter grades, one per line. Empty line to finish");
            while (true)
            {
                string grade = Console.ReadLine();
                if (grade.Length == 0)
                {
                    break;
                }
                gradeList.Add(grade);
            }
            foreach (string grade in gradeList)
            {
                Console.WriteLine(LetterGradetoNumber(grade));
            }
        }

        // part 2
        static List<int> Fibonacci = new List<int>();
        public static void Fibonacci_Iterative(int len)
        {
            Fibonacci.Add(0);
            Fibonacci.Add(1);
            int a = 0, b = 1, c = 0;
           // Console.Write("{0} {1}", a, b);
            for (int i = 2; i < len; i++)
            {
                c = a + b;
                //Console.Write(" {0}", c);
                a = b;
                b = c;
                Fibonacci.Add(c);
            }
          
        }
        static void FibChecker() {
            Console.WriteLine("Enter a number to check against Fibonacci:");
            string line = Console.ReadLine();
            int value;
            if (!int.TryParse(line, out value))
            {
                Console.WriteLine("Invalid input. Please input a number. Exiting.");
                return; 
            }
            int number = Convert.ToInt32(line);
            if( number < 0)
            {
                Console.WriteLine("You must input a positive integer. Exiting.");
                return;
            }
            Fibonacci_Iterative(number + 3);
            Console.WriteLine();
            if (Fibonacci.Contains(number))
            {
                Console.WriteLine("{0} is a Fibonacci number", number);
            }
            else
            {
                for (int i = 0; i < number+3; i++)
                {
                    if (Fibonacci[i] > number)
                    {
                        Console.WriteLine("{0} is not a Fibonacci number, the next Fibonacci number is {1}", number, Fibonacci[i]);
                        break;
                    }
                    
                }
            }
        }
        static void MatrixMultiplier() { }

        static void Main(string[] args)
        {
            try
            {
                GPACalculator();
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Error: You must enter: A, A-, B+, B, B-, C+, C, D or F.");
            }
            try
            {
                
                FibChecker();
            }
        
            finally
            {
                Console.WriteLine("Press any key to end");
                Console.ReadKey();
            }
        }
    }
}
