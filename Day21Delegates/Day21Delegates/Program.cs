﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day21Delegates
{
    class Program
    {
        //step 1: declare a delegate type
        public delegate void LogMessageDelegate(string s);

        //step 2: declare a field of delegate type
        static LogMessageDelegate Logger; // initially null when empty

        //step 3: create one or more methods whose signatures match that of the delegate
        static void LogToConsole(string msg)
        {
            Console.WriteLine("MSG: " + msg);
        }

        static void Main(string[] args)
        {
            try
            {
                Logger += LogToConsole;

                Logger("This is Sparta!");
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
