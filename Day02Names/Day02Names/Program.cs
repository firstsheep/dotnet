﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02Names
{
    class Program
    {
        /*
          static void ReadInNames()
        {
        List<string> nameList = new List<string>();
        Console.WriteLine("Enter Names.Empty line to finish");
        while(true){
        string name = Console.Readline();
        if(name.Length ==0){
        break;
        }
        nameList.Add(name);
        }
        foreach
        }
         * 
         * 
         * 
         */

        static List<string> nameList = new List<string>();
        const int length = 5;
        static void EnterValues()
        {
            for (int i = 0; i < length; i++)
            {
                Console.Write("Enter a name: ");
                string line = Console.ReadLine();
               
                if (String.IsNullOrEmpty(line))
                {
                    Console.WriteLine("Invalid input. Exiting.");
                    return;
                }
                nameList.Add(line);
                
            }
            foreach (string name in nameList)
            {
                Console.WriteLine(name);
            }
        }
        static void Main(string[] args)
        {
            try {
                EnterValues();
            }
            finally
            {
                Console.WriteLine("Press any key to end");
                Console.ReadKey();
            }
        }
    }
}
