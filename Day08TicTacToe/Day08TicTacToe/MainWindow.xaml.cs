﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day08TicTacToe
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int turn = 1; // 1-player 1; 2-player 2
        char[,] board = new char[3, 3]; // charcters either code 0 for empty, "o" or "x"
        private bool isGameStarted = false;
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Start_Button_Click(object sender, RoutedEventArgs e)
            {
            isGameStarted = !isGameStarted;     // change value;

            btStartAndStop.Content = (isGameStarted ? "Stop" : "Start");

            tbPlayerO.IsEnabled = !isGameStarted;
            tbPlayerX.IsEnabled = !isGameStarted;
        }
        
        private bool IsPlayerWinner(char player)
        {
            // return true IF:
            // - three of 'player' in rows
            // - three of 'player' in columns
            // - two diagnoals of 'player'
            if((bt00.Content == bt01.Content && bt01.Content == bt02.Content) ||
                (bt10.Content == bt11.Content && bt11.Content == bt12.Content) ||
                (bt20.Content == bt21.Content && bt21.Content == bt22.Content) ||
                 (bt00.Content == bt10.Content && bt10.Content == bt20.Content) ||
                 (bt01.Content == bt11.Content && bt11.Content == bt21.Content) ||
                 (bt02.Content == bt12.Content && bt12.Content == bt22.Content) ||
                (bt00.Content == bt11.Content && bt11.Content == bt22.Content) ||
                (bt02.Content == bt11.Content && bt11.Content == bt20.Content))
            {
                return true;
            }
            return false; // otherwise return false
        }
        // display X/O on each button
        private void Bt_any_Click(object sender, RoutedEventArgs e)
        {
            Button bt = (Button)sender;

            if(bt.Content.ToString() != "")
            {
                return;
            }
            //
            // register the move - ids are in bt 'Row Column' format
            int row = int.Parse(bt.Name.Substring(2, 1)); // bt02 gives 0
            int col = int.Parse(bt.Name.Substring(3, 1)); // bt02 gives 2
            if (turn == 1)
            {
                //highlight 
                lbPlayerO.Background = Brushes.Yellow;
                lbPlayerX.Background = Brushes.White;
                bt.Content = "O"; //view
                board[row, col] = 'O'; // model
                turn = 2;
            }
            else
            {//2
                lbPlayerO.Background = Brushes.White;
                lbPlayerX.Background = Brushes.Yellow;
                bt.Content = "X";
                board[row, col] = 'X'; // model
                turn = 1;
            }
            // check if there is a winner
           
            // if no winner - check if board is full - 9 moves were made
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbPlayerO.Text != "" && tbPlayerX.Text != "")
            {
                btStartAndStop.IsEnabled = true;
                return;
            }
            btStartAndStop.IsEnabled = false;
        }
    }
}
