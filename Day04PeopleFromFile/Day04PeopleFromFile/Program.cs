﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04PeopleFromFile
{
    class Person
    {
        /*
        public int Random
        {
            get {
                return new Random().Next(1,100);
            }
        } */
        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }

      

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2)
                {
                    throw new ArgumentOutOfRangeException("Name must be at least 2 characters long");
                }
                _name = value;
            }
        }

        // Age must be between 0-150 (both inclusive)
        private int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new ArgumentOutOfRangeException("Age must be between 0-150");
                }
                _age = value;
            }
        }
        public override string ToString()
        {
            // return base.ToString;
            // return string.Format(("Person: {0} is {1} y.o", Name, Age);
            return "Person: " + Name + " " + Age;
        }
    }
    class Program
    {
        const string file_path = @"..\..\..\people.txt";
        static List<Person> people = new List<Person>();
        static void Main(string[] args)
        {
            try
            {

                string[] item;
                string name1;
                int age1;
                string[] lines = File.ReadAllLines(file_path);
                foreach (string l in lines)
                {
                    try
                    {
                        item = l.Split(';');
                        if (item.Length != 2)
                        {
                            throw new InvalidDataException("Line must have exactly two data items: " + l);
                        }
                        name1 = item[0];
                        age1 = int.Parse(item[1]);

                        Person everyone = new Person(name1, age1);
                        //   everyone.Name = name1;
                        //  everyone.Age = age1;
                        people.Add(everyone);
                    }
                    catch (Exception ex)
                    {
                        if (ex is FormatException || ex is OverflowException || ex is ArgumentOutOfRangeException || ex is InvalidDataException)
                        {
                            Console.WriteLine("Error occured: " + ex.Message);
                        }
                        else
                        {
                            throw ex;
                        }
                    }
                }

                foreach (Person p in people)
                {
                    Console.WriteLine(p);
                    //  Console.WriteLine(p.ToString());
                    //Console.WriteLine("Person: {0} is {1} y.o", p.Name, P.Age);


                }

            }
            catch (IOException ex)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
