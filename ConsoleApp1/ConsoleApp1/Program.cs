﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static double sum, avg;
        static List<double> numList = new List<double>();
        static void EnterValues()
        {
            for (int i = 0; i < 5; i++)
            {
                Console.Write("Enter floating point value: ");
                string line = Console.ReadLine();
                double value;
                if (!double.TryParse(line, out value))
                {
                    Console.WriteLine("Invalid input. Exiting.");
                    Environment.Exit(1); // return is not enough to exit the program
                }
                numList.Add(value);
            }
        }
     
            static void findSum()
        {
            foreach (double num in numList)
            {
                sum += num;
                Console.WriteLine("the sum is " + sum);
            }
        }
        static void findAverage()
        {
            avg = sum / numList.Count;
            Console.WriteLine("the average is " + avg);
        }
        static void findStandardDeviation()
        {
            double sumSD = 0.0;
            foreach (double num in numList)
            {
                double sd = (num - avg) * (num - avg);
               
                sumSD += sd;
                double standardDeviation = Math.Sqrt(sumSD/ numList.Count);

            }
            
        }
        static void Main(string[] args)
        {
            try
            {
                EnterValues();
                findSum();
                findStandardDeviation();
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
