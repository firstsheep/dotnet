﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02Numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.Write("Enter how many numbers to generate: ");
                string countStr = Console.ReadLine();
                int count = Convert.ToInt32(countStr);
                // int count = int.Parse(countStr);
                if (count < 1)
                {
                    Console.WriteLine("Error: you must generate 1 or more numbers");
                    return;
                }

                List<int> numList = new List<int>();
                Random random = new Random();
                for (int n = 0; n < count; n++)
                {
                    int r = random.Next(-101, 101);                    
                    numList.Add(r);
                }

                foreach (int number in numList)
                {
                    Console.WriteLine("{0},",number);
                }
                 foreach (int num in numList)
                {
                    if (num < 0)
                    {
                        Console.WriteLine(num);
                    }
                    
                }



            }
            catch (FormatException ex)
            {
                Console.WriteLine("Error: Value entered must be an integer");
            }
            finally
            {
                Console.WriteLine("Press any key to end");
                Console.ReadKey();
            }
        }
    }
}
