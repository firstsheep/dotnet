﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02ArrayContains
{
    class Program
    {
        
        public static void Concatenate(int[] a1, int[] a2) {
            int[] z = new int[a1.Length + a2.Length];
            a1.CopyTo(z, 0);
            a2.CopyTo(z, a1.Length);
            foreach (int num in z)
            {
                Console.Write(num + " \t");
            }

        }
        public static void PrintDups(int[] a1, int[] a2) {
            foreach (int num in a1)
            {
                int pos = Array.IndexOf(a2, num);
                if (pos > -1)
                {
                    Console.WriteLine("{0}", num);
                }
            }
        }
        public static void RemoveDups(int[] a1, int[] a2) {
            // part1 find how many are non-dup
            int desiredSize = 0;
            for (int i = 0; i < a1.Length; i++)
            {
                int val = a1[i];
                bool isDup = false;
                for (int j = 0; j < a2.Length; j++)
                {
                    if (val == a2[j])
                    {
                        isDup = true;
                        break;
                    }
                }
                if (!isDup)
                {
                    desiredSize++;
                }

            }
            //
            int resultPos = 0;
            int[] result = new int[desiredSize];
            for (int i = 0; i < a1.Length; i++)
            {
                int val = a1[i];
                bool isDup = false;
                for (int j = 0; j < a2.Length; j++)
                {
                    if (val == a2[j])
                    {
                        isDup = true;
                        break;
                    }
                }
                if (!isDup)
                {
                    result[resultPos] = val;
                    resultPos++;
                }

            }
            foreach(int num in result)
            {
                Console.Write("{0} ", num);
            }
            /* int[] terms = new int[a1.Length + a2.Length];
             int count = 0;
             foreach (int num in a1)
             {
                 int pos = Array.IndexOf(a2, num);

                 if (pos < 0 )
                 {

                     terms[count] = num;
                     count++;
                 }
             }
             foreach (int num in a2)
             {
                 int pos = Array.IndexOf(a1, num);

                 if (pos < 0)
                 {

                     terms[count] = num;
                     count++;
                 }
             }

             foreach (int num in terms)
             {
                 Console.Write(num + " \t");
             }
             */
        }
        private static bool isValIn2DArray(int val, int[,] data)
        {
            for (int i = 0; i < data.GetLength(0); i++)
            {
                for (int j = 0; j < data.GetLength(1); j++)
                {
                    if (val == data[i, j])
                    {
                        return true;
                    }
                }

            }
            return false;
        }
       public static void PrintDups(int[,] a1, int[,] a2) {
            for (int i = 0; i < a1.GetLength(0); i++)
            {
                for (int j = 0; j < a1.GetLength(1); j++)
                {
                    if (isValIn2DArray(a1[i,j],a2))
                    {
                        Console.Write("{ 0}", a1[i,j]);
                    }
                }

            }
            Console.WriteLine();
        }
        /* public static void PrintDups(int[,] a1, int[,] a2) {
         foreach(int v1 in a1){
            foreach(int v2 in a2){
            if(v1==v2)
            Console.Write("{0}", v1);
            }
            
           }
         }
         */
        static void Main(string[] args)
        {
            int[] a = { 1, 2, 3, 2 };
            int[] b= { 1, 2, 4};
            Concatenate(a,b);
           
            Console.WriteLine();
            PrintDups(a, b);

            RemoveDups(a, b);
            Console.ReadKey();
        }
    }
}
