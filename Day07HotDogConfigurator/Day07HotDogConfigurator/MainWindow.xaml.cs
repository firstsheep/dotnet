﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07HotDogConfigurator
{
    class Hotdogs
    {

    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
      private static List<Hotdogs> hotdogList= new List<Hotdogs>();
        int index;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //customer name
                string name = tbName.Text;
                if (!Regex.IsMatch(name, @"^[^ ]{2,20}$"))
                {
                    MessageBox.Show("Name invalid, must not be empty, no semicolons");
                    return;
                }

                //date of birth
                string DOB = tbDOB.Text;
                //  string[] date = DOB.Split('-');





                // validate DOB
                // CHECK FORMAT
                if (!Regex.IsMatch(DOB, @"^[0-9]{4}-[0-9]{2}-[0-9]{2}$"))
                {
                    MessageBox.Show("invalid input, format must not be YYYY-MM-DD");
                    throw new InvalidDataException("");
                  //  return;
                }

                // CHECK VALID DATE
                DateTime temp;
                if (!DateTime.TryParse(DOB, out temp))
                {
                    //  DOB = DateTime.Today.ToShortDateString();
                    MessageBox.Show("invalid input, YOU MUST INPUT A valid date");
                    return;
                }

                // CHECK AGE 0-150
                // Save today's date.
                var today = DateTime.Today;
                // DateTime dob = DateTime.Parse(DOB);
                DateTime dob = Convert.ToDateTime(DOB);
                // Calculate the age.
                int age = today.Year - dob.Year;
                if (age < 0 || age > 150)
                {
                    MessageBox.Show("invalid input, age must between 0-150");
                    return;
                }
                // bun type
                string bunType = "";
                if (rbRegular.IsChecked == true)
                {
                    bunType = "regular";
                }
                else if (rbWholeGrain.IsChecked == true)
                {
                    bunType = "whole grain";
                }
                else if (rbVegetarian.IsChecked == true)
                {
                    bunType = "vegetarian";
                }
                else
                {
                    // WTF internal error
                    MessageBox.Show("internal errors");
                    return;
                }

                //sausage type
                string sausageType = "";
                if (rbGerman.IsChecked == true)
                {
                    sausageType = "regular";
                }
                else if (rbCheese.IsChecked == true)
                {
                    sausageType = "with cheese";
                }
                else if (rbItalian.IsChecked == true)
                {
                    sausageType = "italian";
                }
                else
                {
                    // WTF internal error
                    MessageBox.Show("internal errors");
                    return;
                }

                // temprature
                int tempC = (int)slTempC.Value;

                // toppings
                List<string> toppingList = new List<string>();
                if (cbKetchup.IsChecked == true)
                {
                    toppingList.Add("Ketchup");
                    // toppingList.Add(cbKetchup.Content.ToString());
                }
                if (cbMustard.IsChecked == true)
                {
                    toppingList.Add("Mustard");
                }
                if (cbRelish.IsChecked == true)
                {
                    toppingList.Add("Relish");
                }
                string toppings = String.Join(",", toppingList);







                File.AppendAllText(@"..\..\..\HotDog.txt", string.Format("{0};{1};{2};{3};{4};{5}{6}", name, DOB, bunType, sausageType, tempC, toppings,
                                Environment.NewLine));
                MessageBox.Show("Data saved");
                tbName.Text = "";
                tbDOB.Text = "";
            }
            catch (FormatException ex)
            {
                MessageBox.Show("[ERROR] Date Format Exception: " + ex.Message);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show("[ERROR] Date of birth must be before than today: " + ex.Message);
            }
            catch (IOException ex)
            {
                MessageBox.Show("[ERROR] Fail to save data: " + ex.Message);
            }
        }

        private void Read_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string[] list = File.ReadAllLines(@"..\..\..\HotDog.txt");
                foreach (string l in list)
                {
                    string[] data = l.Split(';');
                    string name = data[0];
                    int temprature;
                    if(!int.TryParse(data[4], out temprature))
                    {
                        MessageBox.Show("Invalid input");
                        return;
                    }
                    string[] toppingArray = null;
                    if (data[5] != "")
                    {
                        toppingArray = data[5].Split(',');
                    }

                    Hotdogs hotdog = new Hotdogs();
              //      hotdog.Name = name;
                 //   hotdogsList.Add(Hotdog);
                }
                MessageBox.Show("read data successfully");
            }
            catch(InvalidDataException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
