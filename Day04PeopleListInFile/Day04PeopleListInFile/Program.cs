﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04PeopleListInFile
{
    class Person
    {
        /*
        public int Random
        {
            get {
                return new Random().Next(1,100);
            }
        } */
        public Person(string name, int age, string city)
        {
            Name = name;
            Age = age;
            City = city;
        }



        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2)
                {
                    throw new ArgumentOutOfRangeException("Name must be at least 2 characters long");
                }
                _name = value;
            }
        }

        // Age must be between 0-150 (both inclusive)
        private int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new ArgumentOutOfRangeException("Age must be between 0-150");
                }
                _age = value;
            }
        }
        private string _city;
        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                if (value.Length < 1)
                {
                    throw new ArgumentOutOfRangeException("City must be at least 2 characters long");
                }
                _city = value;
            }
        }
        public override string ToString()
        {
            // return base.ToString;
            // return string.Format(("Person: {0} is {1} y.o", Name, Age);
            return "Person: " + Name + " is " + Age + " from " + City;
        }
    }
    class Program
    {

        const string file_path = @"..\..\..\people.txt";
        static List<Person> people = new List<Person>();
        static void AddPersonInfo()
        {
            try
            {
                Console.Write("Adding a person. \n Enter name: ");
                string name_input = Console.ReadLine();
                Console.Write("Enter age: ");
                int age_input = int.Parse(Console.ReadLine());
                // if 
                Console.Write("Enter City: ");
                string city_input = Console.ReadLine();
                Person person_input = new Person(name_input, age_input, city_input);

                people.Add(person_input);
            }
            catch(FormatException ex)
            {
                Console.WriteLine("Error: age must be an integer.");
            }

        }
        static void ListAllPersonsInfo()
        {
            foreach (Person p in people)
            {
                Console.WriteLine(p);
                //  Console.WriteLine(p.ToString());
                //Console.WriteLine("Person: {0} is {1} y.o", p.Name, P.Age);


            }
        }
        static void FindPersonByName()
        {
            Console.WriteLine("Enter partial person name:");
            string name_search = Console.ReadLine();

            foreach (Person item in people)
            {

                if ((item.Name).Contains(name_search))
                    Console.WriteLine("Matches found: {0}", item);
            }



        }
        static void FindPersonYoungerThan()
        {
            try
            {
                Console.WriteLine("Enter maximum age:");

                int age_max = int.Parse(Console.ReadLine());

                if (age_max < 0 || age_max > 150)
                {
                    throw new ArgumentOutOfRangeException("Age must be between 0-150");
                }


                foreach (Person item in people)
                {

                    if ((item.Age) < age_max)
                        Console.WriteLine("Matches found: {0}", item);
                }
            }
            catch (Exception ex)
            {
                if (ex is FormatException || ex is ArgumentOutOfRangeException || ex is InvalidDataException)
                {
                    Console.WriteLine("Error occured: " + ex.Message);
                }
                else
                {
                    throw ex;
                }
            }


        }
        static void ReadAddPeopleFromFile()
        {
            try
            {

                string[] item;
                string name1;
                int age1;
                string city1;
                string[] lines = File.ReadAllLines(file_path);
                foreach (string l in lines)
                {
                    try
                    {
                        item = l.Split(';');
                        if (item.Length != 3)
                        {
                            throw new InvalidDataException("Line must have exactly three data items: " + l);
                        }
                        name1 = item[0];
                        age1 = int.Parse(item[1]);
                        city1 = item[2];

                        Person everyone = new Person(name1, age1, city1);

                        people.Add(everyone);
                    }
                    catch (Exception ex)
                    {
                        if (ex is FormatException || ex is OverflowException || ex is ArgumentOutOfRangeException || ex is InvalidDataException)
                        {
                            Console.WriteLine("Error occured: " + ex.Message);
                        }
                        else
                        {
                            throw ex;
                        }
                    }
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            }
        }
        static void SaveAllPeopleToFile()
        {
            try
            {
                List<string> contents = new List<string>();
                foreach (Person p in people)
                {
                    contents.Add(String.Format("{0};{1};{2}", p.Name, p.Age, p.City));

                }
                File.WriteAllLines(file_path, contents);
                /*foreach (Person p in people)
                {
                    File.WriteAllText(file_path, p.Name + ";" + p.Age + ";" + p.City);
                }*/
                // change a list in to a string
                // Person[] peopleList = people.ToArray();
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error: Fail to save" + ex.Message);
            }

        }
        static int GetUserMenuChoice()
        {
            while (true)
            {
                Console.WriteLine(
                    "What do you want to do?\n" +
                    "1.Add person info\n" +
                    "2.List persons info\n" +
                    "3.Find and list a person by name\n" +
                    "4.Find and list persons younger than age\n" +
                    "0.Exit\n" +
                    "Choice: ");
                string line = Console.ReadLine();
                int choice;
                if (int.TryParse(line, out choice))
                {
                    if (choice >= 0 && choice <= 4)
                    {
                        return choice;
                    }
                }
                Console.WriteLine("Invalid choice\n");
            }
        }

        static void Main(string[] args)
        {
            try
            {
                ReadAddPeopleFromFile();
                //    Console.Write("What do you want to do? \n 1.Add person info \n 2.List persons info \n 3.Find a person by name \n 4.Find all persons younger than age \n 0.Exit");
                //    int choice = int.Parse(Console.ReadLine());
                while (true)
                {
                    int choice = GetUserMenuChoice();

                    switch (choice)
                    {

                        case 1:
                            AddPersonInfo();
                            break;
                        case 2:
                            ListAllPersonsInfo();
                            break;
                        case 3:
                            FindPersonByName();
                            break;
                        case 4:
                            FindPersonYoungerThan();
                            break;
                        case 0:
                            SaveAllPeopleToFile();
                            Environment.Exit(1); //not recommended
                            
                            break;
                            
                        default:
                            Console.WriteLine("Invalid choice try again.");
                            break;
                    }
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}

