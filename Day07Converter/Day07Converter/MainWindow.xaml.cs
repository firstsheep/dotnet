﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07Converter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string celsiusStr = tbName.Text;
            double celsius;
            if(! double.TryParse(celsiusStr, out celsius))
            {
                MessageBox.Show("Invalid input:value must be numerical ");
            }
            double fahrenheit = celsius * 9 / 5 + 32;
           // MessageBox.Show(string.Format(" {0} F", fahrenheit));
            lblResult.Content = string.Format("{0:0.00} F",fahrenheit) ; //{0:0.##}
        }
        
    }
}
