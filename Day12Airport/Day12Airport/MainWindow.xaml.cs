﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day12Airport
{
    public class Airport
    {
        public Airport(string dataLine)
        { // de-serialization
            string[] data = dataLine.Split(';');
            if (data.Length != 5)
            {
                throw new InvalidDataException("Data line must be composed of 3 items exactly");
            }
            // TODO: I may want to add task length verification
            Code = data[0];
            City = data[1];
            try
            {
                Latitude = double.Parse(data[2]);
                Longitude = double.Parse(data[3]);
                ElevationMeters = int.Parse(data[4]);
            }
            catch (FormatException ex)
            { // exception chaining - translating one exception into another
                throw new InvalidDataException("Data format invalid", ex);
            }
        }
        public Airport() { }
        public string Code; // exactly 3 upper case letters
        public string City; // 1-100 characters, excluding ; (semicolons)
        public double Latitude, Longitude; // check wikipedia
        public int ElevationMeters; // -1000 to 10000
        public override string ToString() {
            return String.Format("{0} locates in {1}, latitude is {2} and longitude is {3}, elevationMeters is {4}", Code, City, Latitude, Longitude, ElevationMeters);
        }
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public const string FILE_NAME = @"..\..\..\airportList.dat";
        List<Airport> AirportList = new List<Airport>();
      //  bool hasUnsavedData = false;
        public MainWindow()
        {
            InitializeComponent();
             AirportList.Add(new Airport() { Code = "YUL", City = "Montreal", Latitude = 20.33, Longitude = 31.33, ElevationMeters = 123 });
            AirportList.Add(new Airport() { Code = "DOC", City = "Dockers", Latitude = 120, Longitude = 166, ElevationMeters = -123 });
           
            //   LoadFromFile(FILE_NAME);
            lvAirports.ItemsSource = AirportList;
        }

        public void SaveDataToFile(string fileName, List<Airport> itemsList)
        {
            List<string> linesList = new List<string>();
            try
            {
                foreach (Airport airport in itemsList)
                {
                    linesList.Add(string.Format("{0};{1};{2};{3};{4}", airport.Code, airport.City, airport.Latitude,
                        airport.Longitude, airport.ElevationMeters));
                }
                File.WriteAllLines(fileName, linesList);
                lbStatusBar.Text = string.Format("Wrote {0} airports to file {1}", itemsList.Count, fileName);
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error saving to file: " + ex.Message, "Todo List", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void LoadDataFromFile(string fileName)
        {
            try
            {
                string[] linesList = File.ReadAllLines(fileName);
                foreach (string line in linesList)
                {
                    try
                    {
                        AirportList.Add(new Airport(line));
                    }
                    catch (InvalidDataException ex)
                    {
                        // FIXME: Collect errors and only show a single error message box after the loop
                        MessageBox.Show("Error reading line from file: " + ex.Message, "Todo List", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch (FileNotFoundException ex) { } // ignore
            catch (IOException ex)
            {
                MessageBox.Show("Error saving to file: " + ex.Message, "Todo List", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void FileExportSelected_MenuClick(object sender, RoutedEventArgs e)
        {
            if (lvAirports.SelectedItems.Count == 0)
            {
                MessageBox.Show("You must select one or more items for export first", "Airports List", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "Airport Lists|*.airorts|All files|*.*";
            dialog.Title = "Export airports";
            dialog.ShowDialog();

            if (dialog.FileName != "")
            {
                try
                {
                    List<Airport> selectedItemsList = new List<Airport>();
                    foreach (Airport item in lvAirports.SelectedItems)
                    {
                        selectedItemsList.Add(item);
                    }
                   SaveDataToFile(dialog.FileName, selectedItemsList);
                    MessageBox.Show("Data saved");
                 //   hasUnsavedData = false;
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error saving to file: " + ex.Message, "Airport List", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void FileImport_MenuClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Airport Lists|*.airorts|All files|*.*";
            dialog.Title = "Import airports";
            dialog.ShowDialog();

            // If the file name is not an empty string open it for saving.  
            if (dialog.FileName != "")
            {
                try
                {
                    LoadDataFromFile(dialog.FileName);
                    lvAirports.Items.Refresh();
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error loading data from file: " + ex.Message, "Todo List", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void LvAirports_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void SortByCode_MenuClick(object sender, RoutedEventArgs e)
        {
            AirportList = AirportList.OrderBy(Airport => Airport.Code).ToList<Airport>();
            lvAirports.ItemsSource = AirportList;
        }
    }
}
