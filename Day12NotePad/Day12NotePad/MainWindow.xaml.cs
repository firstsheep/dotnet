﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day12NotePad
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string CurrentlyOpenFilePath = ""; // no file is open
        bool isDocumentModified = false;

        public MainWindow()
        {
            InitializeComponent();
            Title = "Untitled";
            lbStatusBar.Text = "No file open";
        }

        public void SaveDataToFile(string fileName)
        {
           
            try
            {
                File.WriteAllText(fileName, tbDocument.Text);
                lbStatusBar.Text = string.Format("Wrote text to file {0}", fileName);
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error saving to file: " + ex.Message, "Note Pad", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

    

        private void TbDocument_TextChanged(object sender, TextChangedEventArgs e)
        {
            isDocumentModified = true;
            // TODO: update window title here as well
            lbStatusBar.Text = "You are modifying this text";
        }

        private void MenuItem_New_Click(object sender, RoutedEventArgs e)
        {
            if (isDocumentModified)
            {
                // for notepad
                switch (MessageBox.Show("Do you want to save your data?", "Note Pad", MessageBoxButton.YesNoCancel, MessageBoxImage.Information, MessageBoxResult.Cancel))
                {
                    case MessageBoxResult.Yes:
                        // display save dialog
                        /*        SaveFileDialog saveDlg = new SaveFileDialog();
                                saveDlg.ShowDialog();
                               if (saveDlg.FileName != "")
                               {
                                   try
                                   {                      
                                       SaveDataToFile(saveDlg.FileName);
                                       lbStatusBar.Text = "Data Saved";
                                   }
                                   catch (IOException ex)
                                   {
                                       MessageBox.Show("Error saving to file: " + ex.Message, "Todo List", MessageBoxButton.OK, MessageBoxImage.Error);
                                   }
                               }
                               */
                        MenuItem_SaveAs_Click(sender, e);
                        break;
                    case MessageBoxResult.No:
                        // create a new file
                        tbDocument.Text = "";
                        CurrentlyOpenFilePath = "";
                        Title = "";
                        lbStatusBar.Text = "You create a new file";
                        break;
                    case MessageBoxResult.Cancel:
                       
                        break;
                    default:
                        MessageBox.Show("Internal error");
                        break;
                }

            }
       /*     else
            {
                // create a new file
                tbDocument.Text = "";
                CurrentlyOpenFilePath = "";
                Title = "";
                lbStatusBar.Text = "You create a new file";
            }
            */
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (isDocumentModified)
            {
                // for notepad
                switch (MessageBox.Show("Are you sure to exit?", "Note Pad", MessageBoxButton.YesNoCancel, MessageBoxImage.Information, MessageBoxResult.Cancel))
                {
                    case MessageBoxResult.Yes:
                        // close program
                        
                        break;
                    case MessageBoxResult.No:
                        e.Cancel = true;
                        break;
                    case MessageBoxResult.Cancel:
                        e.Cancel = true;
                        break;
                    default:
                        MessageBox.Show("Internal error");
                        break;
                }

            }
           
        }
        public void openFile()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "text|*.txt|All files|*.*";
            dialog.Title = "Open a file";
            dialog.ShowDialog();

            // If the file name is not an empty string open it for saving.  
            if (dialog.FileName != "")
            {
                try
                {
                    LoadDataFromFile(dialog.FileName);
                    isDocumentModified = false;
                    lbStatusBar.Text = string.Format("You opened this file: {0}", dialog.FileName); ;
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error loading data from file: " + ex.Message, "Note Pad", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        private void MenuItem_Open_Click(object sender, RoutedEventArgs e)
        {
            if (isDocumentModified)
            {
                // for notepad
                switch (MessageBox.Show("You have unsaved data. Are you sure to open a new file?", "Note Pad", MessageBoxButton.YesNoCancel, MessageBoxImage.Information, MessageBoxResult.Cancel))
                {
                    case MessageBoxResult.Yes:
                        openFile();
                        break;
                    case MessageBoxResult.No:
                        
                        break;
                    case MessageBoxResult.Cancel:
                        
                        break;
                    default:
                        MessageBox.Show("Internal error");
                        break;
                }

            }
            else
            {
                openFile();
            }
            
        }
        public void LoadDataFromFile(string fileName)
        {
            try
            {
                string text = File.ReadAllText(fileName);
                    
                
                    try
                    {
                    tbDocument.Text = text;
                    }
                    catch (InvalidDataException ex)
                    {
                        // FIXME: Collect errors and only show a single error message box after the loop
                        MessageBox.Show("Error reading line from file: " + ex.Message, "Note Pad", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                
            }
            catch (FileNotFoundException ex) { } // ignore
            catch (IOException ex)
            {
                MessageBox.Show("Error saving to file: " + ex.Message, "Note Pad", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void MenuItem_Exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void MenuItem_SaveAs_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveDlg = new SaveFileDialog();
            saveDlg.Filter = "text|*.txt|All files|*.*";
            saveDlg.Title = "Save it as";
            saveDlg.ShowDialog();
            if (saveDlg.FileName != "")
            {
                try
                {
                    SaveDataToFile(saveDlg.FileName);
                    lbStatusBar.Text = "Data Saved";
                    CurrentlyOpenFilePath = saveDlg.FileName;
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error saving to file: " + ex.Message, "Note Pad", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void MenuItem_Save_Click(object sender, RoutedEventArgs e)
        {
            if(CurrentlyOpenFilePath == "")
            {
                /* SaveFileDialog saveDlg = new SaveFileDialog();
                 saveDlg.Filter = "text|*.txt|All files|*.*";
                 saveDlg.Title = "Save it as";
                 saveDlg.ShowDialog();
                 if (saveDlg.FileName != "")
                 {
                     try
                     {
                         SaveDataToFile(saveDlg.FileName);
                         lbStatusBar.Text = "Data Saved";
                     }
                     catch (IOException ex)
                     {
                         MessageBox.Show("Error saving to file: " + ex.Message, "Note Pad", MessageBoxButton.OK, MessageBoxImage.Error);
                     }
                 }
                 */
                MenuItem_SaveAs_Click(sender, e);
                
            }
            else
            {
                SaveDataToFile(CurrentlyOpenFilePath);
                lbStatusBar.Text = string.Format("Data saved again to {0}", CurrentlyOpenFilePath);
                isDocumentModified = false;
            }
        }
    }

}
