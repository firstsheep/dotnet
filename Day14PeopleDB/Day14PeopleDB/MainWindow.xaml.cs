﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day14PeopleDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Person> PeopleList = new List<Person>();
        
        public MainWindow()
        {
         //   PeopleList.Add(new Person() { Id = 1, Name = "Jerry", Age = 33 });
            InitializeComponent();
            lvPeople.ItemsSource = PeopleList;
           
            // 1. connect to database
            try
            {
                Globals.Db = new Database();
                // 2. load data in ListView
                ReloadList();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL Query\n", "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }
           
        }

        private void ReloadList()
        {
            try
            {

                List<Person> list = Globals.Db.GetAllPeople();
                PeopleList.Clear();
                foreach (Person p in list)
                {
                    PeopleList.Add(p);
                }
                lvPeople.Items.Refresh();
            }catch(SqlException ex)
            {
                MessageBox.Show("Fatal Error: Unable to connect to database\n", "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
                Close(); // close the main window
            }
        }
        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {

        }
        private void AddPerson_MenuClick(object sender, RoutedEventArgs e)
        {
            // new window, instantiate
           AddEditDialog addEditDialog = new AddEditDialog(this);
            if (addEditDialog.ShowDialog() == true)
            {
                ReloadList();
            }
        }

        private void RemovePersonContextMenu_Click(object sender, RoutedEventArgs e)
        {
            // lvPeople.Items.Remove(lvPeople.SelectedItem);  // remove the selected Item 
            Person curperson = lvPeople.SelectedItem as Person;
            if (curperson == null) return;
            MessageBoxResult result = MessageBox.Show(this, "Are you sure you want to delete:\n" +
                curperson, "Confirm delete", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            //
            if (result == MessageBoxResult.OK)
            {
                try
                {
                    Globals.Db.DeletePerson(curperson.Id);
                    ReloadList();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("Error executing SQL query:\n" + ex.Message,
                        "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

        }

        private void LvPeople_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Person curperson = lvPeople.SelectedItem as Person;
            if (curperson == null) return;
            AddEditDialog addEditDialog = new AddEditDialog(this, curperson);
            if(addEditDialog.ShowDialog() == true)
            {
                ReloadList();
            }
        }

        private void EditPersonContextMenu_Click(object sender, RoutedEventArgs e)
        {
            LvPeople_MouseDoubleClick(sender, null);
        }

        // sort when click on the column
        GridViewColumnHeader _lastHeaderClicked = null;
        ListSortDirection _lastDirection = ListSortDirection.Ascending;
        private void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader headerClicked = e.OriginalSource as GridViewColumnHeader;
            ListSortDirection direction;

            if (headerClicked != null)
            {
                if (headerClicked.Role != GridViewColumnHeaderRole.Padding)
                {
                    if (headerClicked != _lastHeaderClicked)
                    {
                        direction = ListSortDirection.Ascending;
                    }
                    else
                    {
                        if (_lastDirection == ListSortDirection.Ascending)
                        {
                            direction = ListSortDirection.Descending;
                        }
                        else
                        {
                            direction = ListSortDirection.Ascending;
                        }
                    }

                    string header = headerClicked.Column.Header as string;
                    Sort(header, direction);

                    _lastHeaderClicked = headerClicked;
                    _lastDirection = direction;
                }
            }
        }

        private void Sort(string sortBy, ListSortDirection direction)
        {
            ICollectionView dataView =
              CollectionViewSource.GetDefaultView(lvPeople.ItemsSource);

            dataView.SortDescriptions.Clear();
            SortDescription sd = new SortDescription(sortBy, direction);
            dataView.SortDescriptions.Add(sd);
            dataView.Refresh();
        }
    }
}
