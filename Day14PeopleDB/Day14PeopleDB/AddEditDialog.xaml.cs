﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day14PeopleDB
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        Person EditedPerson;
        // constructor
        public AddEditDialog(Window owner, Person editedPerson = null)
        {
            InitializeComponent();
            Owner = owner;
            EditedPerson = editedPerson;
            if(EditedPerson != null)
            {
                btAddEdit.Content = "UPdate";
                lblId.Content = EditedPerson.Id;
                tbName.Text = EditedPerson.Name;
                tbAge.Text = EditedPerson.Age + "";
            }

        }

        private void ButtonAddEdit_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            string ageStr = tbAge.Text;

            
            try
            {
               int age = int.Parse(ageStr);        
            
            if (EditedPerson == null)
            {
                    //adding
                    Person person = new Person() { Name = name, Age = age };
                    try
                    {
                        
                        Globals.Db.AddPerson(person);
                    }
                    catch (SqlException ex)
                    {
                        MessageBox.Show("Error executing SQL Query\n", "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                   
                }
            else
            {
                    //updating
                    EditedPerson.Name = name;
                    EditedPerson.Age = age;
                    try
                    {
                        
                        //handle SqlException
                        Globals.Db.UpdatePerson(EditedPerson);
                    }
                    catch (SqlException ex)
                    {
                        MessageBox.Show("Error executing SQL Query\n", "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            DialogResult = true;
            }

            catch (FormatException ex)
            {
                MessageBox.Show("Invalid Input" + ex.Message);
            }
        }
    }
}
