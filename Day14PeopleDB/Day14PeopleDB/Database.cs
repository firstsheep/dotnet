﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day14PeopleDB
{
    public class Database
    {
        const String DbConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\ipd\Documents\FirstDB.mdf;Integrated Security=True;Connect Timeout=30";
        private SqlConnection conn;

        public Database()
        {
            conn = new SqlConnection(DbConnectionString);
            conn.Open();
        }
        public List<Person> GetAllPeople() {
            List<Person> list = new List<Person>();
            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM People", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader[0];
                    string name = (string)reader[1];
                    int age = (int)reader[2];
                    list.Add(new Person() { Id = id, Name = name, Age = age });
                }
            }
            return list;
        }
        public int AddPerson(Person person)
        {
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO People(Name, Age) OUTPUT INSERTED.ID VALUES (@Name, @Age)", conn);
            cmdInsert.Parameters.AddWithValue("Name", person.Name);
            cmdInsert.Parameters.AddWithValue("Age", person.Age);
            int insertId = (int)cmdInsert.ExecuteScalar();
            return insertId;
        }
        public bool UpdatePerson(Person person)
        {
            
            SqlCommand cmdUpdate = new SqlCommand(
                "UPDATE People SET Name = @Name, Age = @Age where Id=@id ", conn);
            cmdUpdate.Parameters.AddWithValue("Id", person.Id);
            cmdUpdate.Parameters.AddWithValue("Name", person.Name);
            cmdUpdate.Parameters.AddWithValue("Age", person.Age);
            int rowsAffect = cmdUpdate.ExecuteNonQuery();
            return rowsAffect > 0;
        }
        public bool DeletePerson(int id) {
            
            SqlCommand cmdDelete = new SqlCommand(
                "DELETE FROM People where Id=@id ", conn);

            cmdDelete.Parameters.AddWithValue("Id", id);
            int rowsAffect = cmdDelete.ExecuteNonQuery();
            return rowsAffect > 0;
        }
    }
}
