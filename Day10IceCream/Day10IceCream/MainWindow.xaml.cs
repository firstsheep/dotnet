﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day10IceCream
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Add_Click(object sender, RoutedEventArgs e)
        {
            /* // option 1:
             String text = lv1.SelectedItem.ToString();          
             string text1 = text.Substring(38);
             lv2.Items.Add(text1);
           */
           
            //option 3:
            ListViewItem item = lv1.SelectedItem as ListViewItem;
            if (item == null)
            {
                MessageBox.Show("Select a flavour first!", "Scoop Selector", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }
            // Option 2: add string directly to the list
            //lv2.Items.Add(item.Content);
            // Option 3: with ListViewItem
            ListViewItem dupItem = new ListViewItem();
            dupItem.Content = item.Content;
            lv2.Items.Add(dupItem); // */
        }


       private void Button_Clear_Click(object sender, RoutedEventArgs e)
       {
           lv2.Items.Clear();
       }

       private void Button_Delete_Click(object sender, RoutedEventArgs e)
       {
            /* // option 1:
          var selected = lv2.SelectedItems.Cast<Object>().ToArray();
          foreach (var item in selected)
          {
              lv2.Items.Remove(item);
          }
          */
         // option 2:      string item = lv2.SelectedItem as string;
            //option 3:
            ListViewItem item = lv2.SelectedItem as ListViewItem;
            if (item == null)
            {
                MessageBox.Show("Select a flavour first!", "Scoop Selector", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }
            lv2.Items.RemoveAt(lv2.SelectedIndex);
           
        }
    }
}
