﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz1Library
{
    public class Book
    {
        public Book() { }
        public Book(string dataLine)
        {
            //de-serialization
            string[] data = dataLine.Split(';');
            if (data.Length != 5)
            {
                throw new DataInvalidException("Data line must be composed of 5 items exactly");
            }

            Title = data[0];
            if (!Regex.IsMatch(Title, @"^[^;]{1,100}$"))
            {
                MessageBox.Show("Title input is invalid, must be 1-100 characters, no semicolons");
                return;
            }
            Author = data[1];
            if (!Regex.IsMatch(Author, @"^[^;]{1,100}$"))
                {
                    MessageBox.Show("Author input invalid, must be 1-100 characters, no semicolons");
                    return;
                }
            Genre = data[2];
           if( Genre != "Classic" && Genre != "Drama" && Genre != "Crime")
            {
                MessageBox.Show("Genre is not in the list");
            }
            double.TryParse(data[3], out double price);
           if( price < 100 || price > 1000)
            {
                MessageBox.Show("Price is not in the range");
            }
            Price = price;

            PubDate = DateTime.ParseExact(data[4], "yyyy-MM-dd", null);
            if (PubDate == null)
            {
                MessageBox.Show("Please choose a date", "Book list", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }


        } // throws DataInvalidException
        public string Author, Title; // 1-100 characters, no semicolons
        public string Genre; // drop-box: Romance, Self-Help, Other
        public double Price; // 0-10000, display with 2 decimal places
        public DateTime PubDate; // publication date - DatePicker
        public override string ToString()
        {
            return String.Format("{0} by {1} is {2} (${3}) pub at {4}", Title, Author, Genre, Price, PubDate.ToShortDateString());

        }
        public string ToDataString()
        {
            return String.Format("{0};{1};{2};{3};{4}", Title, Author, Genre, Price, PubDate.ToString("yyyy-MM-dd"));
        }
    }

    public class DataInvalidException : Exception
    {
        public DataInvalidException(String msg) : base(msg) { }
        public DataInvalidException(String msg, Exception ex) : base(msg, ex) { }
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public const string FILE_NAME = @"library.dat";
        List<Book> bookList = new List<Book>();
        //    bool hasUnsavedData = false;
        public MainWindow()
        {
            InitializeComponent();
            bookList.Add(new Book() { Title = "Buy milk", Author = "Alice", Genre = "Classic", Price = 20.22, PubDate = DateTime.ParseExact("2018-10-27", "yyyy-mm-dd", null) });
            bookList.Add(new Book() { Title = "lalaa", Author = "Ben", Genre = "Crime", Price = 120.22, PubDate = DateTime.ParseExact("2019-03-27", "yyyy-mm-dd", null) });

            LoadDataFromFile(FILE_NAME);
            lvbooks.ItemsSource = bookList;
        }

        void SaveDataToFile(string filePath, List<Book> dataList)
        {
            List<string> linesList = new List<string>();
            try
            {
                foreach (Book book in dataList)
                {
                    linesList.Add(book.ToDataString());

                }
                File.WriteAllLines(filePath, linesList);

                lbStatusBar.Text = string.Format("Export {0} books to file {1}", dataList.Count, filePath);
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error saving to file: " + ex.Message, "Book List", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        void LoadDataFromFile(string filePath)
        {
            try
            {

                string[] filelines = File.ReadAllLines(filePath);

                foreach (string line in filelines)
                {
                    try
                    {
                        bookList.Add(new Book(line));
                    }
                    catch (DataInvalidException ex)
                    {
                        MessageBox.Show("Error reading line from file: " + ex.Message, "Book List", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }

            }
            catch (FileNotFoundException ex) { } // ignore
            catch (IOException ex)
            {
                MessageBox.Show("Error saving to file: " + ex.Message, "Todo List", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void FileExportSelected_MenuClick(object sender, RoutedEventArgs e)
        {
            if (lvbooks.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please select at least one book", "Book List", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }


            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Books|*.books|All Files|*.*";
            saveFileDialog1.Title = "Save a book File";
            saveFileDialog1.ShowDialog();


            if (saveFileDialog1.FileName != "")
            { //TODO: save data to file, semicolon separated
                List<Book> linesList = new List<Book>();
                try
                {

                    foreach (Book book in lvbooks.SelectedItems)
                    {
                        linesList.Add(book);
                    }

                    //  File.AppendAllLines(saveFileDialog1.FileName, linesList);
                    SaveDataToFile(saveFileDialog1.FileName, linesList);
                    MessageBox.Show("Data saved");
                    //   hasUnsavedData = false;

                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error saving to file: " + ex.Message);
                }
            }
        }

        private void Button_AddUpdateTask_Click(object sender, RoutedEventArgs e)
        {
            if (!Regex.IsMatch(tbTitle.Text, @"^[^;]{1,100}$"))
            {
                MessageBox.Show("Title input invalid, must be 1-100 characters, no semicolons");
                return;
            }
            if (!Regex.IsMatch(tbAuthor.Text, @"^[^;]{1,100}$"))
            {
                MessageBox.Show("Author input invalid, must be 1-100 characters, no semicolons");
                return;
            }
            string genre = coboGenre.Text;
            if (genre != "Classic" && genre != "Drama" && genre != "Crime" )
            {
                MessageBox.Show("you must choose from the dropdown list");
                return;
            }
           
            double price = (double)slPrice.Value;
            if (price < 100 || price > 1000)
            {
                MessageBox.Show("Price is not in the range, must between 100 to 1000");
            }
            price = Math.Round(price, 2);
            DateTime date = dpDate.SelectedDate.Value;
            if (date == null)
            {
                MessageBox.Show("Please choose a date", "Book list", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }

            Button senderButton = sender as Button;
            if (senderButton.Name == "btUpdateTask")
            {
                Book item = lvbooks.SelectedItem as Book;
                item.Title = tbTitle.Text;
                item.Author = tbAuthor.Text;
                item.Genre = genre;
                item.Price = price;
                item.PubDate = date;

                lbStatusBar.Text = "Update 1 book to the list";
            }
            else
            {
                Book item = new Book() { Title = tbTitle.Text, Author = tbAuthor.Text, Genre = genre, Price = price, PubDate = date };
                bookList.Add(item);

                //      hasUnsavedData = true;
                // clean up GUI inputs
                tbTitle.Text = "";
                tbAuthor.Text = "";
                coboGenre.Text = "";
                slPrice.Value = 500.00;
                dpDate.SelectedDate = DateTime.Today;

                lbStatusBar.Text = "Add 1 book to the list";
            }
            lvbooks.Items.Refresh();
        }

        private void Lvbooks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Book item = lvbooks.SelectedItem as Book;
            if (item == null)
            {
                // nothing selected
                btUpdateTask.IsEnabled = false;
                btDeleteTask.IsEnabled = false;
                return;
            }
            btUpdateTask.IsEnabled = true;
            btDeleteTask.IsEnabled = true;
            //FIXME
            tbTitle.Text = item.Title;
            tbAuthor.Text = item.Author;
            coboGenre.Text = item.Genre;
            slPrice.Value = item.Price;
            dpDate.SelectedDate = item.PubDate;


        }

        private void BtDeleteTask_Click(object sender, RoutedEventArgs e)
        {
            Book item = lvbooks.SelectedItem as Book;
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete:\n" + item.ToString(), "Book list", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if (result == MessageBoxResult.OK)
            {
                bookList.Remove(item);
                lvbooks.Items.Refresh();
            }
            lbStatusBar.Text = "You deleted 1 book";

        }

        private void Window_Closed(object sender, EventArgs e)
        {
            SaveDataToFile(FILE_NAME, bookList);
        }
    }
}
