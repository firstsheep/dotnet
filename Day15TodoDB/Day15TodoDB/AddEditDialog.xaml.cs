﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day15TodoDB
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        TodoItem EditedTodo;
      
        public AddEditDialog(Window owner, TodoItem editedTodo = null)
        {
            InitializeComponent();
            Owner = owner;
            EditedTodo = editedTodo;
            if (EditedTodo != null)
            {
                btAddEdit.Content = "UPdate";
                lblId.Content = EditedTodo.Id;
                tbTask.Text = EditedTodo.Task;
                dpDate.SelectedDate = EditedTodo.DueDate;
                // rbDone.IsChecked = true;
                rbDone.IsChecked = EditedTodo.IsDone == TaskStatus.Done;
            }

        }
       private void BtAddEdit_Click(object sender, RoutedEventArgs e)
        {
            string task = tbTask.Text;
            DateTime dueDate = dpDate.SelectedDate.Value;
            TaskStatus isDone = rbDone.IsChecked == true ? TaskStatus.Done : TaskStatus.Pending;
            
           
                if (EditedTodo == null)
                {
                    //adding
                    TodoItem TodoItem = new TodoItem() { Task = task, DueDate = dueDate, IsDone = isDone};
                    try
                    {

                        Global.Db.AddTodo(TodoItem);
                    }
                    catch (SqlException ex)
                    {
                        MessageBox.Show("Error executing SQL Query\n", "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    }

                }
                else
                {
                    //updating
                    EditedTodo.Task = task;
                    EditedTodo.DueDate = dueDate;
                    EditedTodo.IsDone = isDone;
                    try
                    {

                        //handle SqlException
                        Global.Db.UpdateTodo(EditedTodo);
                    }
                    catch (SqlException ex)
                    {
                        MessageBox.Show("Error executing SQL Query\n", "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                DialogResult = true;
            }

            
        
    }
}
