﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day15TodoDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<TodoItem> TodoList = new List<TodoItem>();

        public MainWindow()
        {
            //   PeopleList.Add(new TodoItem() { Id = 1, Name = "Jerry", Age = 33 });
            InitializeComponent();
            lvTodos.ItemsSource = TodoList;

            // 1. connect to database
            try
            {
                Global.Db = new Database();
                // 2. load data in ListView
                ReloadList();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL Query\n", "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        private void ReloadList()
        {
            try
            {

                List<TodoItem> list = Global.Db.GetAllTodo();
                TodoList.Clear();
                foreach (TodoItem todo in list)
                {
                    TodoList.Add(todo);
                }
                lvTodos.Items.Refresh();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Fatal Error: Unable to connect to database\n", "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
                Close(); // close the main window
            }
        }

        private void AddMenuItem_Click(object sender, RoutedEventArgs e)
        {
            // new window, instantiate
            AddEditDialog addEditDialog = new AddEditDialog(this);
            if (addEditDialog.ShowDialog() == true)
            {
                ReloadList();
            }
        }

        private void LvTodos_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TodoItem curTodoItem = lvTodos.SelectedItem as TodoItem;
            if (curTodoItem == null) return;
            AddEditDialog addEditDialog = new AddEditDialog(this, curTodoItem);
            if (addEditDialog.ShowDialog() == true)
            {
                ReloadList();
            }
        }
    }
}
