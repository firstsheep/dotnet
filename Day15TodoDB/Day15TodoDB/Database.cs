﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day15TodoDB
{
    class Database
    {
        // school
        const String DbConnectionString = @"Data Source = (LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\ipd\Documents\TodoApp.mdf;Integrated Security = True; Connect Timeout = 30";
        // home
       // const String DbConnectionString = @"Data Source = (LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\User\Documents\TodoApp.mdf;Integrated Security = True; Connect Timeout = 30";

        private SqlConnection conn;
        
        public Database()
        {
            conn = new SqlConnection(DbConnectionString);
            conn.Open();
        }
        public List<TodoItem> GetAllTodo() /*string orderBy = "Id"*/
        {
            List<TodoItem> list = new List<TodoItem>();
            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM Todos", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader[0];
                    string task = (string)reader[1];
                    DateTime duedate = (DateTime)reader[2];
                    //FIXME
                    // bool isDone = (int)reader[3] != 0 ;
                    // bool isDone = true;
                    TaskStatus isDone = (TaskStatus)Enum.Parse(typeof(TaskStatus), (string)reader[3]);
                    list.Add(new TodoItem() { Id = id, Task = task, DueDate = duedate, IsDone = isDone });
                }
            }
            return list;
        }
        public int AddTodo(TodoItem todo)
        {
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO Todos(Task, DueDate, IsDone) OUTPUT INSERTED.ID VALUES (@Task, @DueDate, @IsDone)", conn);
            cmdInsert.Parameters.AddWithValue("Task", todo.Task);
            cmdInsert.Parameters.AddWithValue("DueDate", todo.DueDate);
            cmdInsert.Parameters.AddWithValue("IsDone", todo.IsDone.ToString());
            int insertId = (int)cmdInsert.ExecuteScalar();
            return insertId;
        }
        public bool UpdateTodo(TodoItem todo)
        {

            SqlCommand cmdUpdate = new SqlCommand(
                "UPDATE Todos SET Task = @Task, DueDate = @DueDate, IsDone = @IsDone where Id=@id ", conn);
            cmdUpdate.Parameters.AddWithValue("Id", todo.Id);
            cmdUpdate.Parameters.AddWithValue("Task", todo.Task);
            cmdUpdate.Parameters.AddWithValue("DueDate", todo.DueDate);
            cmdUpdate.Parameters.AddWithValue("IsDone", todo.IsDone);
            int rowsAffect = cmdUpdate.ExecuteNonQuery();
            return rowsAffect > 0;
        }
        public bool DeleteTodo(int id)
        {

            SqlCommand cmdDelete = new SqlCommand(
                "DELETE FROM Todos where Id=@id ", conn);

            cmdDelete.Parameters.AddWithValue("Id", id);
            int rowsAffect = cmdDelete.ExecuteNonQuery();
            return rowsAffect > 0;
        }
    }
}
