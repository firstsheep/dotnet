﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day15TodoDB
{
    public class TodoItem
    {
      public int Id  { get; set; }
      public String Task{ get; set; }
        public DateTime DueDate { get; set; }
        public TaskStatus IsDone { get; set; }
       
        public override string ToString()
        {
            return string.Format("Todo({0}): {1} should be done by {2}, status: {3}", Id, Task, DueDate, IsDone);
        }
    }
    public enum TaskStatus { Pending, Done }
}
