﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07TaskAllInput
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            /*    if(tbName.Text == "")
                {
                    MessageBox.Show("Name mustn't be empty");
                    return;
                }*/
            string name = tbName.Text;
            if (!Regex.IsMatch(name, @"^[^;]{1,}$"))
            {
                MessageBox.Show("Name invalid, must not be empty, no semicolons");
                return;
            }
            string age = "";
            if (rb18.IsChecked == true)
            {
                age = "below 18";
            } else if (rb35.IsChecked == true)
            {
                age = "18-35";
            }
            else if (rb36.IsChecked == true)
            {
                age = "36 and up";
            }
            else
            {
                // WTF internal error
                MessageBox.Show("internal errors");
                return;
            }

            List<string> animalsList = new List<string>();
            if (cbCat.IsChecked == true)
            {
                animalsList.Add("Cat");
            }
            if (cbDog.IsChecked == true)
            {
                animalsList.Add("Dog");
            }
            if (cbOther.IsChecked == true)
            {
                animalsList.Add("Other");
            }
            string animals = String.Join(",", animalsList);
            //

            string continent = zhou.Text;

            int tempC = (int)slValue.Value;


            File.AppendAllText(@"..\..\..\people.txt", string.Format("{0};{1};{2};{3};{4}{5}", name, age, animals, continent, tempC,
                            Environment.NewLine));
            MessageBox.Show("Data saved");
            tbName.Text = "";
        }
    }
}
