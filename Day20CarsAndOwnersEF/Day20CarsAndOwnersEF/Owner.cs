﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day20CarsAndOwnersEF
{
    // [Table("Owner")]
    public class Owner
    {
        // [Key]
        public int Id { get; set; }
        
        [MaxLength(100)]
        public string Name { get; set; }
       public /*virtual*/ ICollection<Car> CarsCollection { get; set; }
        public Owner()
        {
            CarsCollection = new HashSet<Car>();
        }
        
    }
}
