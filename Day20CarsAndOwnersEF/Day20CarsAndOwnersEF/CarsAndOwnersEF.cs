﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day20CarsAndOwnersEF
{
    public class CarsAndOwnersEF2: DbContext
    {
        public CarsAndOwnersEF2() : base() { }
        virtual public DbSet<Car> Cars { get; set; }
        virtual public DbSet<Owner> Owners { get; set; }
    }
}
