﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day20CarsAndOwnersEF
{
    class Program
    {
        static CarsAndOwnersEF2 ctx;
        static int GetUserMenuChoice()
        {
            while (true)
            {
                Console.WriteLine(
                    "What do you want to do?\n" +
                    "1.List all cars and their owner\n" +
                    "2.List all owners and their cars\n" +
                    "3.Add a car (no owner)\n" +
                    "4.Add an owner (no cars)\n" +
                    "5. Assign car to a new owner (or no owner)\n" +
                    "6. Delete an owner with all cars they own\n" +
                    "0.Exit\n" +
                    "Choice: ");
                string line = Console.ReadLine();
                int choice;
                if (int.TryParse(line, out choice))
                {
                    if (choice >= 0 && choice <= 6)
                    {
                        Console.WriteLine();
                        return choice;
                    }
                }
                Console.WriteLine("Invalid choice\n");
            }
        }
        static void AddACarNoOwner()
        {
            
                // Equivalent of INSERT
                Console.WriteLine("Enter a Car Make:");
                string make = Console.ReadLine();
                Console.WriteLine("Enter a year of production of the Car:");
                string yearStr = Console.ReadLine();
                int year;
                if (!int.TryParse(yearStr, out year))
                {
                    Console.WriteLine("Error: year must be an integer");
                    return;
                }
                //use regex
                Car c1 = new Car() { MakeModel = make, YearOfProd = year };

                ctx.Cars.Add(c1);
                ctx.SaveChanges();
        }
        private static void ListAllOwnersAndTheirCars()
        {
            // var list = from o in ctx.Owners select o; //lazy loading (add "virtual" in Owner)
            // eager loading
            var list = ctx.Owners.Include("CarsCollection").ToList();
            foreach (Owner o in list)
            {
                Console.WriteLine("Owner({0}): {1} has {2} cars", o.Id, o.Name, o.CarsCollection.Count);
                foreach (Car c in o.CarsCollection)
                {
                    Console.WriteLine("    Car({0}): {1} made in {2}",
                   c.Id, c.MakeModel, c.YearOfProd);
                }
            }
        }
        private static void ListAllCarsAndTheirOwners()
        {
            List<Car> carsList = ctx.Cars.ToList();
            foreach (Car c in carsList)
            {
                string ownerInfo = c.Owner == null ? "nobody" : c.Owner.Id + "-" + c.Owner.Name;
                Console.WriteLine("Car({0}): {1} made in {2} owned by {3}",
                    c.Id, c.MakeModel, c.YearOfProd, ownerInfo);
            }
        }
        static void AddOwnerNoCar()
        {
           
                // Equivalent of INSERT
                Console.WriteLine("Enter the name of an owner:");
                string name = Console.ReadLine();
                Owner o1 = new Owner() { Name = name };

                ctx.Owners.Add(o1);
                ctx.SaveChanges();
           
        }
        private static void AssignCarToNewOwner()
        {
            ListAllCarsAndTheirOwners();
            // 
            Console.Write("Enter car Id to assign: ");
            int carId = int.Parse(Console.ReadLine());
            Console.Write("Enter owner Id to assign (0 to remove owner): "); // TODO: handle 0
            int ownerId = int.Parse(Console.ReadLine());
            //
            Car selCar = ctx.Cars.Where(car => car.Id == carId).SingleOrDefault();
            Owner selOwner = ctx.Owners.Where(owner => owner.Id == ownerId).SingleOrDefault();
            if (selCar == null || selOwner == null)
            {
                Console.WriteLine("Car or Owner with specified Id not found");
                return;
            }
            selCar.Owner = selOwner; // just update one direction for now, see if it works
            ctx.SaveChanges();
            Console.WriteLine("Record updated");
        }
        private static void DeleteOwnerAndAllCarsTheyOwn()
        {
            ListAllOwnersAndTheirCars();
            Console.Write("Enter owner Id to remove: "); 
            int ownerId = int.Parse(Console.ReadLine());
            Owner selOwner = ctx.Owners.Where(owner => owner.Id == ownerId).SingleOrDefault();
            if (selOwner == null)
            {
                Console.WriteLine("Owner with specified Id not found");
                return;
            }
            ctx.Owners.Remove(selOwner);
            ctx.SaveChanges();

        }
        static void Main(string[] args)
        {
            ctx = new CarsAndOwnersEF2();
            try
            {
                int choice =0;
                do
                {
                    try
                    {
                        choice = GetUserMenuChoice();
                        switch (choice)
                        {
                            case 1:
                                ListAllCarsAndTheirOwners();
                                break;
                            case 2:
                                ListAllOwnersAndTheirCars();
                                break;
                            case 3:
                                AddACarNoOwner();
                                break;
                            case 4:
                                AddOwnerNoCar();
                                break;
                            case 5:
                                AssignCarToNewOwner();
                                break;
                            case 6:
                                DeleteOwnerAndAllCarsTheyOwn();
                                break;
                            case 0:
                                Console.WriteLine("Good bye.");
                                break;
                            default:
                                // yes, you must have a default handler - always
                                Console.WriteLine("Internal error: unknown choice.");
                                break;
                        }
                        Console.WriteLine();
                    }catch(DataMisalignedException ex)
                    {
                        Console.WriteLine("DataException: " + ex.Message);
                    }
                } while (choice != 0);

            }
            finally
            {
                Console.WriteLine("press a key to end");
                Console.ReadKey();
            }
        }
    }
}
