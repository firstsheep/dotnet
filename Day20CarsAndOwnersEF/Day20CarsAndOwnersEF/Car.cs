﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day20CarsAndOwnersEF
{
    // [Table("Car")]
    public class Car
    {
        // [Key]
        public int Id { get; set; }
        // [Column("TheName")]
      
        [MaxLength(100)]
        public string MakeModel { get; set; }
        public int YearOfProd { get; set; }
        //  int OwnerId { get; set; }
        public virtual Owner Owner { get; set; } // relation, make nullable
                                                 /*   public override string ToString()
                                                    {
                                                        return string.Format("Person({0}): {1} is {2} y/o", Id, Name, Age);
                                                    }*/
    }
}
