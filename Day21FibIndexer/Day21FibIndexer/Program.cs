﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day21FibIndexer
{

    public class FibNumbers
    {
        public long this[int index]
        {

            get
            {
                if (index < 1)
                {
                    throw new IndexOutOfRangeException("Fibs only exist for positive numbers");
                }
                return getFib(index);
                /*   int a = 0, b = 1, c = 1;

                   for (int i = 1; i < index; i++)
                   {
                       c = a + b;
                     //  Console.Write(" {0}", c);
                       a = b;
                       b = c;
                   }
                   return c;   
                   */
            }
        }
            private long getFib(int n)
        {
            if (n == 1 || n == 2)
            {
                return 1;
            }
            return getFib(n - 1) + getFib(n - 2);
        }


    

    }
        class Program
    {
        static void Main(string[] args)
        {
            try
            {
                FibNumbers fib = new FibNumbers();
               
                Console.WriteLine(fib[1]); // writes: 1
                Console.WriteLine(fib[2]); // writes: 1
                Console.WriteLine(fib[3]); // writes: 2
                Console.WriteLine(fib[4]); // writes: 3
                Console.WriteLine(fib[5]); // writes: 5

Console.WriteLine(fib[-4]); // throws IllegalAgumentException or similar

            }
            
            finally
            {
                Console.WriteLine("Press any key to finish");
                Console.ReadKey();
            }
        }
    }
}
