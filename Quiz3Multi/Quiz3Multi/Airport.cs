﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3Multi
{
    public class Airport
    {
        public int Id { get; set; }
        public string Code { get; set; } // 3 letters
        [MaxLength(50)][MinLength(0)]
        public string City { get; set; } // 0-50 letters
        public virtual ICollection<Flight> FlightsList { get; set; }
        public Airport()
        {
            FlightsList = new HashSet<Flight>();
        }
    }
}
