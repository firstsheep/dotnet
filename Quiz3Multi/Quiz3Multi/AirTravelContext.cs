﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3Multi
{
    public class AirTravelContext: DbContext
    {
        public AirTravelContext() : base() { }
        virtual public DbSet<Airport> Airports { get; set; }
        virtual public DbSet<Flight> Flights { get; set; }
    }
}
