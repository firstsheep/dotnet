﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz3Multi
{
    public class Program
    {
        static AirTravelContext ctx;
        static void Main(string[] args)
        {
            ctx = new AirTravelContext();
            try
            {
                int choice = 0;
                do
                {
                    try
                    {
                        choice = GetUserMenuChoice();
                        switch (choice)
                        {
                            case 1:
                                AddAirport();
                                break;
                            case 2:
                                RegisterAFlight();
                                break;
                            case 3:
                                ShowAllFlightsInvolvingASpecificAirport();
                                break;
                            case 4:
                                DeleteAnAirportAndAllItsFlights();
                                break;
                            case 5:
                                NthSuperFibNumberr();
                                break;
                            case 6:
                                ChangeExceptionHandlersDelegates();
                                break;
                            case 0:
                                Console.WriteLine("Good bye.");
                                break;
                            default:
                                // yes, you must have a default handler - always
                                Console.WriteLine("Internal error: unknown choice.");
                                break;
                        }
                        Console.WriteLine();
                    }
                    catch (DataMisalignedException ex)
                    {
                        Console.WriteLine("DataException: " + ex.Message);
                    }
                } while (choice != 0);

            }
            finally
            {
                Console.WriteLine("press a key to end");
                Console.ReadKey();
            }

        }
    
        public class FibNumbers
        {
            public long this[int index]
            {

                get
                {
                    if (index < 0)
                    {
                        throw new IndexOutOfRangeException("Fibs only exist for non-negative numbers");
                    }
                    return getFib(index);
                    
                }
            }
            private long getFib(int n)
            {
                if( n == 0)
                {
                    return 0;
                }
                if (n == 1 || n == 2)
                {
                    return 1;
                }
                return getFib(n - 1) + getFib(n - 2) + getFib(n - 3);


            }

            
        }
       
        private static void NthSuperFibNumberr()
        {
            try
            {
                Console.Write("Which super-fib would you like to see?");
                string line = Console.ReadLine();
                int n = int.Parse(line);
                FibNumbers fibn = new FibNumbers();
                Console.WriteLine(fibn[n]);

                

            }
            catch (FormatException ex)
            {
                //Logger(ex.Message);
                Console.WriteLine("Invalid choice " + ex.Message);
            }
             catch (IndexOutOfRangeException ex )
            {
                //Logger(ex.Message);
               Console.WriteLine("Invalid choice " + ex.Message);
            }
            

            }

        private static void ChangeExceptionHandlersDelegates()
        {
            try
            {
                Console.WriteLine(
                        "1.to console / screen\n" +
                        "2.to file\n" +
                        "3.to database\n" +
                         "Choice: ");
                string line = Console.ReadLine();
                List<string> choices = new List<string>();
              //  choices = line.Split(',');

              //  int choice = int.Parse(line);
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Invalid choice\n" + ex.Message);
            }
            Logger += LogToScreen;
            Logger += LogToFile;
            Logger += LogToDatabase;
        }

        private static void DeleteAnAirportAndAllItsFlights()
        {
            try
            {
                ShowAllAirports();
                try
                {
                    Console.Write("Enter airport Id to delete: ");
                    int airportId = int.Parse(Console.ReadLine());


                    Airport airportDelete = ctx.Airports.Where(airport => airport.Id == airportId).SingleOrDefault();
                    if (airportDelete == null)
                    {
                        Console.WriteLine("It doesn't exist, input again");
                        return;
                    }
                    ctx.Airports.Remove(airportDelete);

                }
                catch (FormatException)
                {
                    Console.WriteLine("invalid input");
                }
                ctx.SaveChanges();
            }
            catch (Exception)
            {
                Console.WriteLine("Exception");
            }
        }

        private static void ShowAllFlightsInvolvingASpecificAirport()
        {
            ShowAllAirports();
            Console.WriteLine("Enter an aiport code:");
            string code = Console.ReadLine();
            if (!Regex.IsMatch(code, @"^[A-Za-z]{3}$"))
            {
                Console.WriteLine("code invalid, must be 3 leters");
                return;
            }
            Airport airportInput = ctx.Airports.Where(airport => airport.Code == code).SingleOrDefault();
            if (airportInput == null)
            {
                Console.WriteLine("It doesn't exist, input again");
                return;
            }


            foreach (Flight flight in ctx.Flights)
            {
                if (flight.FromCity == airportInput || flight.ToCity == airportInput)
                {
                    Console.WriteLine("    Flight({0}): Flight Number-{1} Flight From {2}, Flight To {3}",
               flight.Id, flight.Number, flight.FromCity.City, flight.ToCity.City);
                }
            }

        }
        static void ShowAllAirports()
        {
            var airportList = from A in ctx.Airports select A;
            Console.WriteLine("Now we have these airports:");
            foreach (Airport a in airportList)
            {
                Console.WriteLine("Airport: {0}, {1}, {2}", a.Id, a.Code, a.City);
            }
        }

        private static void RegisterAFlight()
        {
            ShowAllAirports();
            Console.WriteLine("Enter a flight number:");
            string number = Console.ReadLine();
            if (!Regex.IsMatch(number, @"^[A-Z]{2}[0-9]{3}$"))
            {
                Console.WriteLine("Number invalid, must be wo letters followed by two or three digits, e.g. UA234");
                return;
            }
            Console.WriteLine("Enter the name of city from:");
            string fromCity = Console.ReadLine();
            Airport fromA = ctx.Airports.Where(airport => airport.City == fromCity).SingleOrDefault();
            if (fromA == null)
            {
                Console.WriteLine("It doesn't exist, add airport first");
                return;
            }
            Console.WriteLine("Enter the name of city to:");
            string toCity = Console.ReadLine();
            Airport toA = ctx.Airports.Where(airport => airport.City == toCity).SingleOrDefault();
            if (toA == null)
            {
                Console.WriteLine("It doesn't exist, add airport first");
                return;
            }
            Flight fa = new Flight() { Number = number, FromCity = fromA, ToCity = toA };

            ctx.Flights.Add(fa);
            ctx.SaveChanges();
            Console.WriteLine("Add flight successfully");
        }

        private static void AddAirport()
        {
            // Equivalent of INSERT
            Console.WriteLine("Enter an aiport code:");
            string code = Console.ReadLine();
            if (!Regex.IsMatch(code, @"^[A-Za-z]{3}$"))
            {
                Console.WriteLine("code invalid, must be 3 leters");
                return;
            }
            Console.WriteLine("Enter an city name:");
            string city = Console.ReadLine();
            if (!Regex.IsMatch(city, @"^[A-Za-z]{0,50}$"))
            {
                Console.WriteLine("city invalid, must be 0-50 leters");
                return;
            }

            Airport a = new Airport() { Code = code, City = city };

            ctx.Airports.Add(a);
            ctx.SaveChanges();
            Console.WriteLine("Add Airport successfully");
        }

        static int GetUserMenuChoice()
        {
            while (true)
            {
                Console.WriteLine(
                    "1.Add airport\n" +
                    "2.Register a flight from/to airports\n" +
                    "3.Show all flights involving a specific airport (from or to)\n" +
                    "4.Delete an airport and all flights associated with it\n" +
                    "5.Display Nth SuperFib number\n" +
                    "6.Change exception handlers delegates\n" +
                    "0.Exit\n" +
                    "Choice: ");
                string line = Console.ReadLine();
                int choice;
                if (int.TryParse(line, out choice))
                {
                    if (choice >= 0 && choice <= 6)
                    {
                        Console.WriteLine();
                        return choice;
                    }
                }
                Console.WriteLine("Invalid choice\n");
            }
        }

        //step 1: declare a delegate type
        public delegate void LogMessage(string s);

        //step 2: declare a field of delegate type
        static LogMessage Logger; // initially null when empty

        public static void LogToScreen(string msg)
        {
            Console.WriteLine("MSG: " + msg);
        }
        // display exception or other error message to the screen

        public static void LogToFile(string msg)
        {
            Console.WriteLine("MSG: " + msg);
        }
        // save exception or other error message to file @"..\..\..\main.log"
        // Note: Every line in file must be preceeded by a timestamp, e.g.
        //2019-05-09 09:06:52 Exception: unable to connect to database

        public static void LogToDatabase(string msg)
        {
            Console.WriteLine("MSG: " + msg);
        }
        // save exception or other error message to database using LogMessage and EntityFramework
    }
}
