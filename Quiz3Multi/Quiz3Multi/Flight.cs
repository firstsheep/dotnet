﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3Multi
{
    public class Flight
    {
        public int Id { get; set; }
        public string Number { get; set; } // two letters followed by two or three digits, e.g. UA234 
        public virtual Airport FromCity { get; set; }
        public virtual Airport ToCity { get; set; }
    }
}
