﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03DayPerson
{
    class Person
    {
        /*
        public int Random
        {
            get {
                return new Random().Next(1,100);
            }
        } */
        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2)
                {
                    throw new ArgumentOutOfRangeException("Name must be at least 2 characters long");
                }
                _name = value;
            }
        }

        // Age must be between 0-150 (both inclusive)
        private int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new ArgumentOutOfRangeException("Age must be between 0-150");
                }
                _age = value;
            }
        }


    }
    class Teacher : Person
    {
        public Teacher(string name, int age, string field, int yoe) : base(name, age)
        {
            this.Field = field;
            this.YOE = yoe;
        }
        private string _field;
        public string Field
        {
            get
            {
                return _field;
            }
            set
            {
                if (value.Length < 2)
                {
                    throw new ArgumentOutOfRangeException("Field must be at least 2 characters long");
                }
                _field = value;
            }
        }
        private int _YOE;
        public int YOE
        {
            get
            {
                return _YOE;
            }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentOutOfRangeException("YOE must be between 0-100");
                }
                _YOE = value;
            }
        }
        class Student : Person
        {
            public Student(string name, int age, string program, double gpa) : base(name, age)
            {
                this.Program = program;
                this.GPA = gpa;
            }
            private string _program;
            public string Program
            {
                get
                {
                    return _program;
                }
                set
                {
                    if (value.Length < 2)
                    {
                        throw new ArgumentOutOfRangeException("_program must be at least 2 characters long");
                    }
                    _program = value;
                }
            }
            private double _GPA;
            public double GPA
            {
                get
                {
                    return _GPA;
                }
                set
                {
                    if (value < 0 || value > 4.0)
                    {
                        throw new ArgumentOutOfRangeException("GPA must be between 0-4.0");
                    }
                    _GPA = value;
                }
            }
        }

        class Program
        {
            static void Main(string[] args)
            {
                try
                {
                    // using an initalizer instead of a constructor
                    //Person p = new Person() { Name = "Jerry", Age = 33 };
                    Person p = new Person("Jerry", 33);
                    // p.Name = "M";
                    p.Age = 77;
                    Console.WriteLine("Person: {0} is {1} y/o", p.Name, p.Age);
                    Teacher Li = new Teacher("Ski", 66, "Computer", 15);
                    Console.WriteLine(Li.Age);
                    Student MY = new Student("myy", 33, "IPD", 3.0);
                    Console.WriteLine(MY.GPA);
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    Console.WriteLine("Exception: {0}", ex.Message);
                    Console.WriteLine(ex.StackTrace);
                }
                finally
                {
                    Console.WriteLine("Press any key to finish");
                    Console.ReadKey();
                }
            }
        }
    }
}


