﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz2PeopleTravel
{
       
    class PersonTravels
    {
        
        public PersonTravels(string name, int age, string passportNo)
        {
            Name = name;
            Age = age;
            PassportNo = passportNo;          
        }


        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                if (value.Length < 2 || value.Length > 50)
                {
                    throw new InvalidDataException("Name must be 2-50 characters long");
                }
                _name = value;
            }
        }
        // Age must be between 0-150 (both inclusive)
        private int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new InvalidDataException("Age must be between 0-150");
                }
                _age = value;
            }
        }
        private string _PassportNo;
        public string PassportNo
        {
            get { return _PassportNo; }
            set
            {
                // to check two uppercase letters followed by six digits exactly - use regexp
                if (!Regex.Match(value, "^[A-Z]{2}[0-9]{6}$").Success)
                {
                    throw new InvalidDataException("PassportNo must be two uppercase letters followed by six digits exactl");
                }

                _PassportNo = value;
            }
        }

        
       List<string> _countriesVisited = new List<string>();
        public List<string> CountriesVisited
        {
            get { return _countriesVisited; }
            set { _countriesVisited = value; }
        }
        public void AddCountryUnique(string country) {

        }
        public override string ToString()
        {
            
                return string.Format("{0} is {1} y/o passport {2} visited: {3}", Name, Age, PassportNo, CountriesVisited);
            
        }
    }
    class Program
    {
        const string FILE_PATH = @"..\..\..\data.txt";

        static List<PersonTravels> people = new List<PersonTravels>();
        //FIXME
        static void AddPeople()
        {
            try
            {
                Console.WriteLine("Adding a person and countries visited.");
                Console.Write("Enter name: ");
                string name = Console.ReadLine();
                Console.Write("Enter age: ");
                string ageStr= Console.ReadLine();
                int age;
                if (!int.TryParse(ageStr, out age))
                {
                    Console.WriteLine("Error: Age must be an integer");
                    return;
                }
                Console.Write("Enter passport: ");
                string passport = Console.ReadLine();
                Console.Write("countries visited, one per line, enter (empty line) ends list: ");
                // FIXME
                string countries = Console.ReadLine();
                PersonTravels p = new PersonTravels(name, age, passport);
                people.Add(p);
                
            }
            catch (InvalidDataException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            catch (FormatException ex)
            {
                Console.WriteLine("Error: latitude and longitude must be a number");
            }
        }
      
    
        // completed
        static void ListAllPeopleInfo()
        {
            Console.WriteLine("Listing all info");
            int count = 1;
            foreach (PersonTravels p in people)
            {
                Console.WriteLine("{0}.{1}",count,p);
                count++;
            }
        }

        static void RegisterNewCountry()
        {
            ListAllPeopleInfo();
            Console.WriteLine("Registering person visiting a new country. ");
            Console.WriteLine("Enter person number to add country to: ");
            string numStr = Console.ReadLine();
            int num;
            if (!int.TryParse(numStr, out num))
            {
                Console.WriteLine("Line corrupted, you must input a number: ");
                return;
            }
            Console.Write("Enter country name:");
            string country = Console.ReadLine();
            string[] contents = File.ReadAllLines(FILE_PATH);
            for (int i = 0; i < contents.Length; i++)
            {
                
            }
            
        }



        // FIXME
        static void ReadAllPeopleFromFile()
        {
            try
            {
                if (!File.Exists(FILE_PATH))
                {
                    return;
                }
                string[] contents = File.ReadAllLines(FILE_PATH);
                foreach (string line in contents)
                {
                    string[] data = line.Split(';');
                    if (data.Length != 4)
                    {
                        Console.WriteLine("Line corrupted, must have 4 fields exactly: " + line);
                        continue;
                    }
                    string name = data[0];
                    int age;
                    if (!int.TryParse(data[1], out age))
                    {
                        Console.WriteLine("Line corrupted, age must be a number: " + line);
                        continue;
                    }

                    string passport = data[2];
                    string[] countryList = data[3].Split(',');
                    List<string> countries = new List<string>();
                    countries.AddRange(countryList);

                    try
                    {
                        PersonTravels p = new PersonTravels(name, age, passport);
                        people.Add(p);
                    }
                    catch (InvalidDataException ex)
                    {
                        Console.WriteLine("Line corrupted: " + line + "\nError:" + ex.Message);
                    }
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error: failed to read data from file: " + ex.Message);
            }
        }
       //COMPLETED
       static void SaveAllPeopleToFile()
        {
            try
            {
                List<string> contents = new List<string>();
                foreach (PersonTravels p in people)
                {
                    contents.Add(String.Format("{0};{1};{2};{3}", p.Name, p.Age, p.PassportNo, p.CountriesVisited));
                }
                File.WriteAllLines(FILE_PATH, contents);
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error: failed to save data to file: " + ex.Message);
            }
        }


            //COMPLETED
        static int GetUserMenuChoice()
        {
            while (true)
            {
                Console.WriteLine("1.Display all people and countries they traveled to\n" +
                    "2.Add person and countries visited\n" +
                    "3.Register a new country visited by a person\n" +
                    "4.Find out who visited most countries\n" +
                    "5. List all who visited a specific country\n" +
                    "0.Exit\n" +
                    "Choice: ");
                string line = Console.ReadLine();
                int choice;
                if (int.TryParse(line, out choice))
                {
                    if (choice >= 0 && choice <= 5)
                    {
                        Console.WriteLine();
                        return choice;
                    }
                }
                Console.WriteLine("Invalid choice\n");
            }
        }

        static void Main(string[] args)
        {
            try
            {
                ReadAllPeopleFromFile();

                int choice;
                do
                {
                    choice = GetUserMenuChoice();
                    switch (choice)
                    {
                        case 1:
                            ListAllPeopleInfo();
                            break;
                        case 2:
                            AddPeople();
                            break;
                        case 3:
                            RegisterNewCountry();
                            break;
                        case 4:
     //                       FindPeopleTravelMost();
                            break;
                        case 5:
       //                     FindPeopleByCountry();
                            break;
                        case 0:
                            Console.WriteLine("Good bye.");
                            break;
                        default:                           
                            Console.WriteLine("Internal error: unknown choice.");
                            break;
                    }
                    Console.WriteLine();
                } while (choice != 0);
               SaveAllPeopleToFile();
            }
            finally
            {
                Console.WriteLine("press a key to end");
                Console.ReadKey();
            }
        }
    }
}

